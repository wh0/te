package w.te;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.GrantPermissionRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import w.te.rear.CommonInit;
import w.te.rear.Host;
import w.te.rear.HostSettings;
import w.te.test.TestCredentials;

public class Utils {

	private Context ourContext;
	private Context targetContext;
	private SharedPreferences hostPrefs;

	@Before
	public void setUpCredentials() throws IOException {
		ourContext = InstrumentationRegistry.getInstrumentation().getContext();
		final InputStream testHostKey = ourContext.getResources().openRawResource(w.te.test.R.raw.ssh_host_rsa_key);
		targetContext = ApplicationProvider.getApplicationContext();
		CommonInit.initFront(targetContext);
		CommonInit.init();
		Host.init();
		final Path hostKeyPath = targetContext.getFilesDir().toPath().resolve(".ssh/ssh_host_rsa_key");
		Files.copy(testHostKey, hostKeyPath, StandardCopyOption.REPLACE_EXISTING);
		hostPrefs = HostSettings.getSharedPreferences();
		hostPrefs.edit()
				.putString("auth_authorized_key", TestCredentials.ID_RSA_PUB)
				.putString("direct_port", "20522")
				.putBoolean("relay_enable", true)
				.putString("relay_nickname", TestCredentials.RELAY_NICKNAME)
				.putString("relay_knock", TestCredentials.RELAY_KNOCK)
				.apply();
	}

	@Rule
	public GrantPermissionRule permissions() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
			return GrantPermissionRule.grant(Manifest.permission.POST_NOTIFICATIONS);
		}
		return GrantPermissionRule.grant();
	}

	@Test
	public void listenRelayed() throws Exception {
		final Path running = targetContext.getCacheDir().toPath().resolve("test-connect-running");
		Files.createDirectories(running);
		HostSettings.registerToken(hostPrefs);
		while (Files.exists(running)) {
			Thread.sleep(1000);
		}
	}

	@Test
	public void listenDirect() throws Exception {
		final Path running = targetContext.getCacheDir().toPath().resolve("test-connect-running");
		Files.createDirectories(running);
		{
			final Intent intent = new Intent(targetContext, HostService.class)
					.setAction(HostService.ACTION_START_SERVER);
			ourContext.startForegroundService(intent);
		}
		while (Files.exists(running)) {
			Thread.sleep(1000);
		}
		{
			final Intent intent = new Intent(targetContext, HostService.class)
					.setAction(HostService.ACTION_STOP_SERVER);
			ourContext.startForegroundService(intent);
		}
	}

}

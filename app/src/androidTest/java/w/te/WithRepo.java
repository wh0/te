package w.te;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import androidx.test.core.app.ApplicationProvider;

import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.internal.storage.file.GC;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectInserter;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.RefUpdate;
import org.eclipse.jgit.revwalk.RevWalk;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import w.te.rear.CommonInit;
import w.te.rear.Repos;

public class WithRepo {

	@Before
	public void setUp() {
		CommonInit.initFront(ApplicationProvider.getApplicationContext());
		CommonInit.init();
		Repos.init();
	}

	@Rule
	public TemporaryFolder repoFolder = new TemporaryFolder();

	@Test
	public void gc() throws Exception {
		try (FileRepository repo = new FileRepository(repoFolder.getRoot())) {
			repo.create(true);
			String tagEmpty = Constants.R_TAGS + "empty";
			try (ObjectInserter oi = repo.newObjectInserter()) {
				ObjectId empty = oi.insert(Constants.OBJ_BLOB, new byte[]{});
				RefUpdate ru = repo.updateRef(tagEmpty);
				ru.setNewObjectId(empty);
				RefUpdate.Result r;
				try (RevWalk rw = new RevWalk(repo)) {
					r = ru.update(rw);
				}
				switch (r) {
					case NO_CHANGE:
					case NEW:
					case FAST_FORWARD:
					case FORCED:
						break;
					default:
						fail();
				}
			}
			assertNotNull(new GC(repo).gc());
			try (ObjectReader or = repo.newObjectReader()) {
				ObjectId empty = repo.exactRef(tagEmpty).getObjectId();
				ObjectLoader ol = or.open(empty);
				assertEquals(0, ol.getSize());
			}
		}
	}

}

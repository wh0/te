package w.te;

import static org.junit.Assert.assertSame;

import androidx.test.core.app.ApplicationProvider;

import org.apache.sshd.common.util.ExceptionUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

import w.te.rear.CommonInit;
import w.te.rear.Host;

public class Fills {

	@Before
	public void setUp() {
		CommonInit.initFront(ApplicationProvider.getApplicationContext());
		CommonInit.init();
		Host.init();
	}

	@Test
	public void peel() {
		final Exception root = new Exception();
		assertSame(ExceptionUtils.peelException(new ExecutionException(root)), root);
	}

}

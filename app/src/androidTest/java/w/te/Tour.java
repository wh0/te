package w.te;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static androidx.test.espresso.matcher.ViewMatchers.hasFocus;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withHint;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.intent.rule.IntentsRule;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class Tour {

	@Rule
	public final ActivityScenarioRule<RepoActivity> asr = new ActivityScenarioRule<>(RepoActivity.class);
	@Rule
	public final IntentsRule ir = new IntentsRule();
	private Context ourContext;
	private Context targetContext;

	@Before
	public void setUp() throws Exception {
		ourContext = InstrumentationRegistry.getInstrumentation().getContext();
		targetContext = ApplicationProvider.getApplicationContext();
	}

	@Test
	public void run() throws Exception {
		openActionBarOverflowOrOptionsMenu(targetContext);
		onView(withText(R.string.host_settings))
				.perform(click());
		Espresso.pressBack();

		openActionBarOverflowOrOptionsMenu(targetContext);
		onView(withText(R.string.new_repo))
				.perform(click());
		onView(allOf(withHint(R.string.hint_name), hasFocus()))
				.perform(replaceText("tour"))
				.perform(typeTextIntoFocusedView("\n"));

		openActionBarOverflowOrOptionsMenu(targetContext);
		onView(withText(R.string.new_file))
				.perform(click());
		onView(allOf(withHint(R.string.hint_path), hasFocus()))
				.perform(replaceText("sample.txt"))
				.perform(typeTextIntoFocusedView("\n"));
		onView(allOf(withId(R.id.edit), hasFocus()))
				.perform(replaceText("hello world!\n"));
		Espresso.closeSoftKeyboard();
		Espresso.pressBack();

		openActionBarOverflowOrOptionsMenu(targetContext);
		onView(withText(R.string.commit))
				.perform(click());

		final Path backup = targetContext.getCacheDir().toPath().resolve("tour.bundle.gpg");
		Files.deleteIfExists(backup);
		openActionBarOverflowOrOptionsMenu(targetContext);
		onView(withText(R.string.backups))
				.perform(click());
		onView(withId(R.id.passphrase))
				.perform(replaceText("hunter2"));
		onView(withId(R.id.create))
				.perform(click());
		final Uri[] uriSlot = new Uri[]{null};
		intending(allOf(
				hasAction(Intent.ACTION_CHOOSER),
				hasExtra(is(Intent.EXTRA_INTENT), hasAction(Intent.ACTION_SEND))
		))
				.respondWithFunction((intent) -> {
					uriSlot[0] = intent
							.<Intent>getParcelableExtra(Intent.EXTRA_INTENT)
							.getParcelableExtra(Intent.EXTRA_STREAM);
					return new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
				});
		onView(withId(R.id.share))
				.perform(click());
		final InputStream is = ourContext.getContentResolver().openInputStream(uriSlot[0]);
		Files.copy(is, backup);
		onView(allOf(withId(R.id.finish), isEnabled()))
				.perform(click());
		Espresso.pressBack();
	}

}

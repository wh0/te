package w.te.test;

public class TestCredentials {

	// corresponds to id_rsa
	public static final String ID_RSA_PUB = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRtAZyAKjakZ8c544jCq1vRsZA0Lq06s/GZvwqZRbONPClEVC31bP9KK7lhmD5OiBFIexhFCmvZxXRK4TZdFBFKYO7RqevfB/BKJn2rgLLGtkLT2SqW/d8VcLd4X6+7y3McvfVdWqG+CApA0uY73LwJ7wjpJ6ngSDK744g11jwWbaSSoeTJEDAmulyp76XoAvLbdsWsr/vWMtdC5TNEx2VGbUroOQZH1dUKuDg+bnjQUXakaL4F5Q+EL96wQcLRauQVbQAmjgNBPmOcxzbCjpO29wEYihNPW8O8PTSPJD/TjoBhGeQALIa/E7SeIiwSVKKeMRjgdAQcsJ2jmpGHwg5 testing";
	public static final String RELAY_NICKNAME = "test_device";
	public static final String RELAY_KNOCK = "knnnock";

}

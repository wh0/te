package w.te.low;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.text.format.DateUtils;
import android.util.AttributeSet;

import w.te.R;

public class LastRegisteredPreference extends Preference {

	public LastRegisteredPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setLastRegistered(long lastRegistered) {
		final String summary;
		if (lastRegistered == 0) {
			summary = getContext().getString(R.string.last_registered_never);
		} else {
			summary = getContext().getString(R.string.last_registered_success, DateUtils.getRelativeTimeSpanString(lastRegistered));
		}
		persistLong(lastRegistered);
		setSummary(summary);
	}

	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return (long) a.getInteger(index, 0);
	}

	@Override
	protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		setLastRegistered(restorePersistedValue ? getPersistedLong(0) : (long) defaultValue);
	}

}

package w.te.low;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

public class KeepableTempFile implements AutoCloseable {

	public final Path path;
	private boolean kept;

	public KeepableTempFile(Path dir, String prefix, String suffix, FileAttribute<?> ...attrs) throws IOException {
		path = Files.createTempFile(dir, prefix, suffix, attrs);
	}

	public OutputStream getOutputStream() throws IOException {
		return Files.newOutputStream(path);
	}

	public Path keep() {
		kept = true;
		return path;
	}

	@Override
	public void close() throws IOException {
		if (!kept) Files.delete(path);
	}

}

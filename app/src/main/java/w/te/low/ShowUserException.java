package w.te.low;

import android.content.Context;
import android.preference.Preference;
import android.widget.Toast;

import java.util.Arrays;

import w.te.rear.Globals;

public class ShowUserException extends Exception {

	private final int resId;
	private final Object[] formatArgs;

	public ShowUserException(int resId, Object... formatArgs) {
		this.resId = resId;
		this.formatArgs = formatArgs;
	}

	@Override
	public String getMessage() {
		final String resName = Globals.applicationContext.getResources().getResourceName(resId);
		return "Localized message without context: " + resName + " " + Arrays.toString(formatArgs);
	}

	public String getMessage(Context context) {
		return context.getString(resId, formatArgs);
	}

	public void showToast(Context context) {
		Toast.makeText(context, getMessage(context), Toast.LENGTH_SHORT).show();
	}

	public void setSummary(Preference pref) {
		pref.setSummary(getMessage(pref.getContext()));
	}

}

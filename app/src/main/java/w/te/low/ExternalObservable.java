package w.te.low;

import java.util.Observable;

public class ExternalObservable extends Observable {

	public void setChangedAndNotifyObservers() {
		setChanged();
		notifyObservers();
	}

}

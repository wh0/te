package w.te.low;

import android.content.Context;
import android.icu.text.ListFormatter;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

import org.apache.sshd.common.config.keys.AuthorizedKeyEntry;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import w.te.R;

public class AuthorizedKeysPreference extends EditTextPreference {

	public AuthorizedKeysPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	private CharSequence summarizeKeys(String authorizedKeysRaw) {
		final List<AuthorizedKeyEntry> authorizedKeys;
		if (authorizedKeysRaw == null) {
			authorizedKeys = Collections.emptyList();
		} else {
			try {
				authorizedKeys = AuthorizedKeyEntry.readAuthorizedKeys(new StringReader(authorizedKeysRaw), true);
			} catch (StreamCorruptedException e) {
				return e.getMessage();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		final List<String> items = new ArrayList<>(authorizedKeys.size());
		int other = 0;
		for (final AuthorizedKeyEntry k : authorizedKeys) {
			final String c = k.getComment();
			if (c == null) {
				other++;
			} else {
				items.add(c);
			}
		}
		if (items.isEmpty()) {
			if (other == 0) return getContext().getString(R.string.authorized_keys_none);
			return getContext().getResources().getQuantityString(R.plurals.uncommented_keys_only, other, other);
		} else {
			if (other > 0) items.add(getContext().getResources().getQuantityString(R.plurals.uncommented_keys, other, other));
			return ListFormatter.getInstance().format(items);
		}
	}

	@Override
	public void setText(String text) {
		super.setText(text);
		setSummary(summarizeKeys(text));
	}

}

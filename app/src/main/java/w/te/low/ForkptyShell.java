package w.te.low;

import android.os.ParcelFileDescriptor;
import android.util.Log;

import org.apache.sshd.common.util.io.input.NullInputStream;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.Signal;
import org.apache.sshd.server.channel.ChannelSession;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.shell.InvertedShell;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ForkptyShell implements InvertedShell {

	private int masterFd = -1;
	private int procPid = -1;
	private int exitStatus = -1;

	private ChannelSession channel;
	private ParcelFileDescriptor master;
	private ServerSession session;

	private native void startNative() throws IOException;
	private native boolean checkStatusNative() throws IOException;
	private native void destroyNative() throws IOException;
	private native void signalNative(int sig) throws IOException;
	private native void setWindowSizeNative(int row, int col, int xpixel, int ypixel) throws IOException;

	static {
		System.loadLibrary("forkpty_shell");
	}

	@Override
	public ChannelSession getServerChannelSession() {
		return channel;
	}

	@Override
	public OutputStream getInputStream() {
		return new FileOutputStream(master.getFileDescriptor());
	}

	@Override
	public InputStream getOutputStream() {
		return new FileInputStream(master.getFileDescriptor());
	}

	@Override
	public InputStream getErrorStream() {
		return new NullInputStream();
	}

	@Override
	public boolean isAlive() {
		if (exitStatus != -1) throw new IllegalStateException();
		final boolean alive;
		try {
			alive = checkStatusNative();
		} catch (IOException e) {
			throw new RuntimeException(e); // %%%
		}
		if (alive) return true;
		procPid = -1;
		return false;
	}

	private void maybeCloseMaster() {
		if (master != null) {
			try {
				master.close();
			} catch (IOException e) {
				throw new RuntimeException(e); // %%%
			}
			master = null;
			masterFd = -1;
		}
	}

	@Override
	public int exitValue() {
		if (exitStatus == -1) throw new IllegalStateException();
		maybeCloseMaster();
		return exitStatus;
	}

	@Override
	public void setSession(ServerSession session) {
		this.session = session;
	}

	@Override
	public ServerSession getServerSession() {
		return session;
	}

	private void updateWindowSize(Environment env) throws IOException {
		final int row = Integer.parseInt(env.getEnv().getOrDefault(Environment.ENV_LINES, "0"));
		final int col = Integer.parseInt(env.getEnv().getOrDefault(Environment.ENV_COLUMNS, "0"));
		setWindowSizeNative(row, col, 0, 0);
	}

	@Override
	public void start(ChannelSession channel, Environment env) throws IOException {
		this.channel = channel;
		if (!env.getEnv().containsKey(Environment.ENV_TERM)) Log.w("w", "Running without tty is not implemented");
		startNative();
		master = ParcelFileDescriptor.adoptFd(masterFd);
		updateWindowSize(env);
		env.addSignalListener((channel1, signal) -> {
			if (signal == Signal.WINCH) {
				try {
					updateWindowSize(env);
				} catch (IOException e) {
					Log.e("w", "Couldn't set window size " + signal, e);
				}
				return;
			}
			try {
				signalNative(signal.getNumeric());
			} catch (IOException e) {
				Log.e("w", "Couldn't signal " + signal, e);
			}
		});
	}

	@Override
	public void destroy(ChannelSession channel) throws Exception {
		if (procPid == -1) return;
		destroyNative();
		maybeCloseMaster();
	}

	@Override
	protected void finalize() throws Throwable {
		if (procPid != -1) {
			Log.e("w", "ForkptyShell leaking pid " + procPid);
		}
	}

}

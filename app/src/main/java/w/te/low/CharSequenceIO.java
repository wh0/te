package w.te.low;

import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class CharSequenceIO {

	public static CharSequence readInputStream(InputStream is, int capacity) throws IOException {
		try (Reader r = new InputStreamReader(is)) {
			final StringBuilder sb = new StringBuilder(capacity);
			final char[] buf = new char[1024];
			while (true) {
				final int amount = r.read(buf);
				if (amount == -1) break;
				sb.append(buf, 0, amount);
			}
			return sb;
		}
	}

	public static void writeOutputStream(OutputStream os, CharSequence data) throws IOException {
		try (Writer w = new OutputStreamWriter(os)) {
			final char[] buf = new char[1024];
			int remaining = data.length();
			int offset = 0;
			while (remaining > 0) {
				final int amount = Math.min(remaining, buf.length);
				TextUtils.getChars(data, offset, offset + amount, buf, 0);
				w.write(buf, 0, amount);
				offset += amount;
				remaining -= amount;
			}
		}
	}

}

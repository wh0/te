package w.te.low;

import android.text.LoginFilter;

public class HostnameFilter extends LoginFilter.UsernameFilterGMail {

	@Override
	public boolean isAllowed(char c) {
		if (super.isAllowed(c)) return true;
		if (c == '-') return true;
		return false;
	}

}

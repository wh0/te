package w.te.low;

import android.util.Log;

import org.apache.sshd.common.AttributeRepository;
import org.apache.sshd.common.config.keys.AuthorizedKeyEntry;
import org.apache.sshd.common.config.keys.KeyUtils;
import org.apache.sshd.common.config.keys.PublicKeyEntryResolver;
import org.apache.sshd.server.auth.AsyncAuthException;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.session.ServerSession;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.List;

public abstract class SessionAuthorizedKeysAuthenticator implements PublickeyAuthenticator {

	public static final AttributeRepository.AttributeKey<AuthorizedKeyEntry> AUTHORIZED_KEY = new AttributeRepository.AttributeKey<>();

	protected abstract List<AuthorizedKeyEntry> getAuthorizedKeys() throws IOException;

	protected void onFail(String username, PublicKey key, ServerSession session) {
	}

	@Override
	public boolean authenticate(String username, PublicKey key, ServerSession session) throws AsyncAuthException {
		final List<AuthorizedKeyEntry> authorizedKeys;
		try {
			authorizedKeys = getAuthorizedKeys();
		} catch (IOException e) {
			Log.w("w", "getAuthorizedKeys failed", e);
			return false;
		}
		for (final AuthorizedKeyEntry ak : authorizedKeys) {
			final PublicKey k;
			try {
				k = ak.resolvePublicKey(session, PublicKeyEntryResolver.FAILING);
			} catch (IOException | GeneralSecurityException e) {
				Log.w("w", "Couldn't resolve public key " + ak);
				continue;
			}
			if (KeyUtils.compareKeys(k, key)) {
				session.setAttribute(AUTHORIZED_KEY, ak);
				return true;
			}
		}
		onFail(username, key, session);
		return false;
	}

}

package w.te.low;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class HookedEditText extends EditText {

	public interface Hooks {
		boolean onKeyPreIme(int keyCode, KeyEvent event);
	}

	public Hooks hooks;

	public HookedEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
		if (hooks != null) {
			if (hooks.onKeyPreIme(keyCode, event)) return true;
		}
		return super.onKeyPreIme(keyCode, event);
	}

}

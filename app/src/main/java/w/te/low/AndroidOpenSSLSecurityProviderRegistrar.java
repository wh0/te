package w.te.low;

import androidx.annotation.Keep;

import org.apache.sshd.common.util.security.AbstractSecurityProviderRegistrar;

import java.security.Provider;
import java.security.Security;

public class AndroidOpenSSLSecurityProviderRegistrar extends AbstractSecurityProviderRegistrar {

	@Keep
	public AndroidOpenSSLSecurityProviderRegistrar() {
		super("AndroidOpenSSL");
	}

	@Override
	public boolean isSupported() {
		return true;
	}

	@Override
	public String getDefaultSecurityEntitySupportValue(Class<?> entityType) {
		return ALL_OPTIONS_VALUE;
	}

	@Override
	public Provider getSecurityProvider() {
		return Security.getProvider("AndroidOpenSSL");
	}

}

package w.te;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.bouncycastle.openpgp.PGPException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import w.te.databinding.ActivityBackupBinding;
import w.te.low.UnsupportedContentProvider;
import w.te.rear.Backups;
import w.te.rear.CommonInit;
import w.te.rear.Repos;

public class BackupActivity extends Activity {

	public static class Provider extends UnsupportedContentProvider {
		@Override
		public boolean onCreate() {
			CommonInit.initFront(getContext().getApplicationContext());
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				CommonInit.init();
				Repos.init();
			});
			return super.onCreate();
		}

		private static String getSuffix(Path diamond) {
			final String fileName = diamond.getFileName().toString();
			final int firstDotIndex = fileName.indexOf('.');
			if (firstDotIndex == -1) throw new IllegalArgumentException();
			return fileName.substring(firstDotIndex);
		}

		@Nullable
		@Override
		public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable Bundle queryArgs, @Nullable CancellationSignal cancellationSignal) {
			final MatrixCursor result = new MatrixCursor(new String[]{
					OpenableColumns.DISPLAY_NAME,
					OpenableColumns.SIZE,
			}, 1);
			final Backups.Bundle bundle = Backups.query(uri.toString());
			if (bundle != null) {
				final String name;
				if (bundle.fromId == null) {
					name = uri.getLastPathSegment() + "-" + bundle.toId.substring(0, 7) + "-full" + getSuffix(bundle.diamond);
				} else {
					name = uri.getLastPathSegment() + "-" + bundle.toId.substring(0, 7) + "-delta-" + bundle.fromId.substring(0, 7) + getSuffix(bundle.diamond);
				}
				final long size;
				try {
					size = Files.size(bundle.diamond);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				result.addRow(new Object[]{
						name,
						size,
				});
			}
			return result;
		}

		@Override
		public String getType(@NonNull Uri uri) {
			return "application/octet-stream";
		}

		@Override
		public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
			if (!"r".equals(mode)) throw new UnsupportedOperationException();
			final Backups.Bundle bundle = Backups.query(uri.toString());
			if (bundle == null) throw new FileNotFoundException();
			return ParcelFileDescriptor.open(bundle.diamond.toFile(), ParcelFileDescriptor.MODE_READ_ONLY);
		}
	}

	private ActivityBackupBinding binding;

	private String repoName;
	private Uri diamondUri;

	private Backups.Bundle bundle;

	private final Backups.Client client = new Backups.Client() {
		@Override
		public void onLastId(String lastId) {
			runOnUiThread(() -> {
				binding.have.setText(lastId);
				binding.delta.setChecked(true);
			});
		}

		@Override
		public void onBackupReady(Backups.Bundle b) {
			bundle = b;
		}

		@Override
		public void onProgressUpdateTask(String title, int work, int totalWork) {
			runOnUiThread(() -> {
				if (totalWork == 0) {
					binding.progressText.setText(getString(R.string.progress_indeterminate, title, work));
					binding.progressWork.setIndeterminate(true);
				} else {
					final int percentage = 100 * work / totalWork;
					binding.progressText.setText(getString(R.string.progress_determinate, title, percentage, work, totalWork));
					binding.progressWork.setIndeterminate(false);
					binding.progressWork.setProgress(work);
					binding.progressWork.setMax(totalWork);
				}
			});
		}

		@Override
		public void onProgressEndTask() {
			runOnUiThread(() -> {
				binding.progressTasks.incrementProgressBy(1);
				binding.progressWork.setIndeterminate(false);
				binding.progressWork.setProgress(1);
				binding.progressWork.setMax(1);
			});
		}

		@Override
		public void onProgressComplete() {
			runOnUiThread(() -> {
				binding.progressTasks.setProgress(5);
			});
		}
	};

	private static final String PREFS_NAME = "backups";

	private String formatBundleInfo() {
		if (bundle.fromId == null) {
			return getString(R.string.bundle_info_full, bundle.toId.substring(0, 7));
		} else {
			return getString(R.string.bundle_info_delta, bundle.fromId.substring(0, 7), bundle.toId.substring(0, 7));
		}
	}

	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
		});
		super.onCreate(savedInstanceState);
		binding = ActivityBackupBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		binding.passphrase.setText(getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString("last_passphrase", ""));
		binding.delta.setOnCheckedChangeListener((buttonView, isChecked) -> {
			binding.have.setEnabled(isChecked);
			if (!isChecked) binding.have.setError(null);
		});
		binding.create.setOnClickListener((v) -> {
			View errorFocus = null;
			final String haveString = binding.have.getText().toString();
			final boolean delta = binding.delta.isChecked();
			if (delta && haveString.isEmpty()) {
				binding.have.setError(getString(R.string.error_backup_create_empty_have));
				if (errorFocus == null) errorFocus = binding.have;
			}
			final String have = delta ? haveString : null;
			final CharSequence passphraseText = binding.passphrase.getText();
			final int passphraseLength = passphraseText.length();
			if (passphraseLength == 0) {
				binding.passphrase.setError(getString(R.string.error_backup_create_empty_passphrase));
				if (errorFocus == null) errorFocus = binding.passphrase;
			}
			final char[] passphrase = new char[passphraseLength];
			TextUtils.getChars(passphraseText, 0, passphraseLength, passphrase, 0);
			if (errorFocus != null) {
				errorFocus.requestFocus();
				return;
			}

			getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit()
					.putString("last_passphrase", passphraseText.toString())
					.apply();
			setFormEnabled(false);
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				try {
					Backups.create(repoName, diamondUri.toString(), have, passphrase, client);
				} catch (IOException | PGPException e) {
					throw new RuntimeException(e);
				} finally {
					Arrays.fill(passphrase, 'x');
				}
				runOnUiThread(() -> {
					binding.progressText.setText(R.string.backup_done);
					binding.bundleInfo.setText(formatBundleInfo());
					setResultEnabled(true);
				});
			});
		});
		binding.share.setOnClickListener((v) -> {
			final Intent intent = new Intent(Intent.ACTION_SEND)
					.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
					.putExtra(Intent.EXTRA_STREAM, diamondUri)
					.setType("application/octet-stream");
			startActivity(Intent.createChooser(intent, getString(R.string.backup_share_title)));
		});
		binding.discard.setOnClickListener((v) -> {
			setResultEnabled(false);
			final Backups.Bundle b = bundle;
			bundle = null;
			binding.progressText.setText("");
			binding.progressTasks.setProgress(0);
			binding.progressWork.setIndeterminate(false);
			binding.progressWork.setProgress(0);
			binding.progressWork.setMax(1);
			binding.bundleInfo.setText("");
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				try {
					Backups.discard(diamondUri.toString(), b.diamond);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				runOnUiThread(() -> {
					setFormEnabled(true);
				});
			});
		});
		binding.finish.setOnClickListener((v) -> {
			setResultEnabled(false);
			final Backups.Bundle b = bundle;
			bundle = null;
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				try {
					Backups.finish(repoName, diamondUri.toString(), b);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				runOnUiThread(() -> {
					finish();
				});
			});
		});

		repoName = getIntent().getStringExtra("repo");
		diamondUri = Uri.parse(Backups.getBackupUri(repoName));

		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			Backups.check(repoName, diamondUri.toString(), client);
			runOnUiThread(() -> {
				if (bundle == null) {
					setFormEnabled(true);
				} else {
					binding.bundleInfo.setText(formatBundleInfo());
					setResultEnabled(true);
				}
			});
		});
	}

	private void setFormEnabled(boolean enabled) {
		binding.have.setEnabled(enabled && binding.delta.isChecked());
		binding.passphrase.setEnabled(enabled);
		binding.delta.setEnabled(enabled);
		binding.create.setEnabled(enabled);
	}

	private void setResultEnabled(boolean enabled) {
		binding.share.setEnabled(enabled);
		binding.discard.setEnabled(enabled);
		binding.finish.setEnabled(enabled);
	}

}

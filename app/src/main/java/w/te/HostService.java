package w.te;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.ListFormatter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.ArrayMap;
import android.util.Log;

import androidx.annotation.Nullable;

import org.apache.sshd.common.config.keys.PublicKeyEntry;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import w.te.low.ExternalObservable;
import w.te.rear.CommonInit;
import w.te.rear.Globals;
import w.te.rear.Host;
import w.te.rear.HostSettings;
import w.te.rear.Repos;

public class HostService extends Service {

	public static final String NOTIFICATION_CHANNEL_JUNK = "junk";
	public static final String NOTIFICATION_CHANNEL_HISTORICAL = "historical";
	public static final String NOTIFICATION_CHANNEL_AUTH = "auth";
	public static final int NOTIFICATION_ACTIVE = 1;
	private static final int NOTIFICATION_HISTORICAL_PUSH = 2;
	private static final int NOTIFICATION_HISTORICAL_SHELL = 3;
	private static final int NOTIFICATION_HISTORICAL_SUMMARY = 4;
	private static final int NOTIFICATION_AUTH_SETUP = 5;
	private static final String NOTIFICATION_GROUP_HISTORICAL_ACTIVITY = "historical_activity";
	public static final String ACTION_ACCEPT_SLOT = "accept_slot";
	public static final String ACTION_START_SERVER = "start_server";
	public static final String ACTION_STOP_SERVER = "stop_server";
	public static final String EXTRA_SLOT = "slot";

	public static final int STATE_STOPPED = 0;
	public static final int STATE_STARTING = 1;
	public static final int STATE_STARTED = 2;
	public static final int STATE_STOPPING = 3;

	public static int directState = STATE_STOPPED;
	public static final ExternalObservable directStateObservable = new ExternalObservable();
	private Host.DirectHost directHost;
	private final Map<Long, String> directConnections = new ArrayMap<>();

	private Host.RelayHost relayHost;
	private int numRelayConnections;

	private boolean startedForeground;
	private int lastStartId;

	private void maybeStop() {
		if (numRelayConnections > 0 || directState != STATE_STOPPED) return;
		startedForeground = false;
		stopForeground(STOP_FOREGROUND_REMOVE);
		stopSelf(lastStartId);
	}

	private Notification getOngoingNotification() {
		// todo: find other hardcoded strings
		final Notification.Builder nb = new Notification.Builder(this, NOTIFICATION_CHANNEL_JUNK);
		nb.setSmallIcon(R.drawable.ic_host);
		nb.setContentTitle(getString(R.string.host_service_notification_active_content_title));
		nb.setOnlyAlertOnce(true);
		if (!directConnections.isEmpty() || numRelayConnections > 0) {
			final List<String> items = new ArrayList<>(directConnections.size() + 1);
			items.addAll(directConnections.values());
			if (numRelayConnections > 0) items.add(getResources().getQuantityString(R.plurals.relay_connections, numRelayConnections, numRelayConnections));
			nb.setStyle(new Notification.BigTextStyle().bigText(getString(R.string.host_service_notification_active_content_text, ListFormatter.getInstance().format(items))));
		}
		return nb.build();
	}

	private void updateOngoingNotification() {
		if (!startedForeground) throw new IllegalStateException();
		getSystemService(NotificationManager.class).notify(NOTIFICATION_ACTIVE, getOngoingNotification());
	}

	private void updateActivitySummaryNotification(NotificationManager nm) {
		final Notification n = new Notification.Builder(this, NOTIFICATION_CHANNEL_HISTORICAL)
				.setSmallIcon(R.drawable.ic_push)
				.setContentTitle(getString(R.string.host_service_notification_historical_summary_content_title))
				.setGroup(NOTIFICATION_GROUP_HISTORICAL_ACTIVITY)
				.setGroupSummary(true)
				.build();
		nm.notify(NOTIFICATION_HISTORICAL_SUMMARY, n);
	}

	public static class AuthorizeKeyService extends IntentService {
		public static final String EXTRA_KEY = "key";

		public AuthorizeKeyService() {
			super("AuthorizeKeyService");
		}

		@Override
		protected void onHandleIntent(@Nullable Intent intent) {
			final String keyStr = intent.getStringExtra(EXTRA_KEY);
			Log.v("w", "AuthorizeKeyService dummy implementation " + keyStr);

			final SharedPreferences prefs = HostSettings.getSharedPreferences();
			final String prevAuthorizedKeys = prefs.getString("auth_authorized_key", null);
			final String authorizedKeys;
			if (prevAuthorizedKeys == null || prevAuthorizedKeys.isEmpty()) {
				authorizedKeys = keyStr;
			} else if (prevAuthorizedKeys.endsWith("\n")) {
				authorizedKeys = prevAuthorizedKeys + keyStr + "\n";
			} else {
				authorizedKeys = prevAuthorizedKeys + "\n" + keyStr;
			}
			prefs.edit()
					.putString("auth_authorized_key", authorizedKeys)
					.apply();
			Globals.mainHandler.post(() -> {
				HostSettings.authorizedKeysObservable.setChangedAndNotifyObservers();
			});

			getSystemService(NotificationManager.class).cancel(keyStr, NOTIFICATION_AUTH_SETUP);
		}
	}

	final Host.Client client = new Host.Client() {
		@Override
		public void onDirectSessionCreated(long id, SocketAddress remoteAddress) {
			Log.v("w", "%%% onDirectSessionCreated " + id);
			final String remoteAddressStr;
			if (remoteAddress instanceof InetSocketAddress) {
				remoteAddressStr = ((InetSocketAddress) remoteAddress).getHostString();
			} else {
				remoteAddressStr = remoteAddress.toString();
			}
			Globals.mainHandler.post(() -> {
				directConnections.put(id, remoteAddressStr);
				updateOngoingNotification();
			});
		}

		@Override
		public void onDirectSessionClosed(long id) {
			Log.v("w", "%%% onDirectSessionClosed " + id);
			Globals.mainHandler.post(() -> {
				directConnections.remove(id);
				updateOngoingNotification();
			});
		}

		@Override
		public void onRelayConnectSuccess(String slot, long id) {
			Log.v("w", "%%% onRelayConnectSuccess " + slot + " into " + id);
		}

		@Override
		public void onRelayConnectFail(String slot) {
			Log.v("w", "%%% onRelayConnectFail " + slot);
			Globals.mainHandler.post(() -> {
				relayFinish();
			});
		}

		@Override
		public void onRelaySessionCreated(long id) {
			Log.v("w", "%%% onRelaySessionCreated " + id);
		}

		@Override
		public void onRelaySessionClosed(long id) {
			Log.v("w", "%%% onRelaySessionClosed " + id);
			Globals.mainHandler.post(() -> {
				relayFinish();
			});
		}

		@Override
		public void onAuthFail(PublicKey publicKey) {
			Log.v("w", "%%% onAuthFail");
			final SharedPreferences prefs = HostSettings.getSharedPreferences();
			final String currentAuthorizedKeys = prefs.getString("auth_authorized_key", null);
			if (currentAuthorizedKeys != null && !currentAuthorizedKeys.isEmpty()) return;
			final String keyStr = PublicKeyEntry.toString(publicKey);
			Log.v("w", "%%% public key entry " + keyStr);
			Globals.mainHandler.post(() -> {
				final Intent intent = new Intent(HostService.this, AuthorizeKeyService.class)
						.setData(Uri.fromParts("x", keyStr, null))
						.putExtra(AuthorizeKeyService.EXTRA_KEY, keyStr);
				final PendingIntent pi = PendingIntent.getService(HostService.this, 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
				final Notification.Action na = new Notification.Action.Builder(null, getString(R.string.host_service_notification_auth_setup_action_authorize), pi)
						.build();
				final Notification n = new Notification.Builder(HostService.this, NOTIFICATION_CHANNEL_AUTH)
						.setSmallIcon(R.drawable.ic_key)
						.setContentTitle(getString(R.string.host_service_notification_auth_setup_content_title))
						.setStyle(new Notification.BigTextStyle().bigText(keyStr))
						.addAction(na)
						.build();
				getSystemService(NotificationManager.class).notify(keyStr, NOTIFICATION_AUTH_SETUP, n);
			});
		}

		@Override
		public void onPrePush(String repoName) {
			Log.v("w", "%%% onPrePush to " + repoName);
		}

		@Override
		public void onPush(String authorizedKeyComment, String repoName) {
			Log.v("w", "%%% onPush from " + authorizedKeyComment + " to " + repoName);
			Globals.mainHandler.post(() -> {
				final String keyStr = authorizedKeyComment == null ? getString(R.string.key_uncommented) : getString(R.string.key_commented, authorizedKeyComment);
				final Intent intent = new Intent(HostService.this, FileListActivity.class)
						.setData(Uri.fromParts("x", repoName, null))
						.putExtra("repo", repoName);
				final PendingIntent pi = PendingIntent.getActivity(HostService.this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
				final Notification n = new Notification.Builder(HostService.this, NOTIFICATION_CHANNEL_HISTORICAL)
						.setSmallIcon(R.drawable.ic_push)
						.setContentTitle(repoName)
						.setContentText(getString(R.string.host_service_notification_historical_push_content_text, keyStr))
						.setContentIntent(pi)
						.setAutoCancel(true)
						.setShowWhen(true)
						.setGroup(NOTIFICATION_GROUP_HISTORICAL_ACTIVITY)
						.build();
				final NotificationManager nm = getSystemService(NotificationManager.class);
				nm.notify(repoName, NOTIFICATION_HISTORICAL_PUSH, n);
				updateActivitySummaryNotification(nm);
			});
		}

		@Override
		public void onPreShell(String authorizedKeyComment) {
			Log.v("w", "%%% onPreShell from " + authorizedKeyComment);
			Globals.mainHandler.post(() -> {
				final String keyStr = authorizedKeyComment == null ? getString(R.string.key_uncommented) : getString(R.string.key_commented, authorizedKeyComment);
				final NotificationManager nm = getSystemService(NotificationManager.class);
				final Notification n = new Notification.Builder(HostService.this, NOTIFICATION_CHANNEL_HISTORICAL)
						.setSmallIcon(R.drawable.ic_push)
						.setContentTitle(getString(R.string.host_service_notification_historical_shell_content_title))
						.setContentText(getString(R.string.host_service_notification_historical_shell_content_text, keyStr))
						.setShowWhen(true)
						.setGroup(NOTIFICATION_GROUP_HISTORICAL_ACTIVITY)
						.build();
				nm.notify(NOTIFICATION_HISTORICAL_SHELL, n);
				updateActivitySummaryNotification(nm);
			});
		}
	};

	private void directStart() {
		if (directState != STATE_STOPPED) {
			Log.v("w", "directStart: state not stopped");
			maybeStop();
			return;
		}
		directState = STATE_STARTING;
		directStateObservable.setChangedAndNotifyObservers();

		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				directHost = Host.createDirect(client);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			Globals.mainHandler.post(() -> {
				directState = STATE_STARTED;
				directStateObservable.setChangedAndNotifyObservers();
			});
		});
	}

	private void directFinish() {
		if (directState != STATE_STARTED) {
			Log.v("w", "directFinish: state not started");
			maybeStop();
			return;
		}
		directState = STATE_STOPPING;
		directStateObservable.setChangedAndNotifyObservers();

		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				directHost.stop();
			} catch (IOException e) {
				Log.w("w", "directHost stop", e);
			}
			directHost = null;
			Globals.mainHandler.post(() -> {
				directState = STATE_STOPPED;
				directStateObservable.setChangedAndNotifyObservers();
				maybeStop();
			});
		});
	}

	private void relayStart(String slot) {
		numRelayConnections++;
		updateOngoingNotification();

		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			if (relayHost == null) relayHost = Host.createRelay(client);
			AsyncTask.THREAD_POOL_EXECUTOR.execute(() -> {
				relayHost.acceptSlot(slot, client);
			});
		});
	}

	private void relayFinish() {
		numRelayConnections--;
		updateOngoingNotification();
		if (numRelayConnections > 0) return;

		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				relayHost.stop();
			} catch (IOException e) {
				Log.e("w", "relayHost stop", e);
			}
			relayHost = null;
			Globals.mainHandler.post(() -> {
				maybeStop();
			});
		});
	}

	@Override
	public void onCreate() {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
			Host.init();
		});
		super.onCreate();

		// create notification channels
		final NotificationManager nm = getSystemService(NotificationManager.class);
		final NotificationChannel ncJunk = new NotificationChannel(NOTIFICATION_CHANNEL_JUNK, getString(R.string.host_service_notification_channel_junk_name), NotificationManager.IMPORTANCE_DEFAULT);
		nm.createNotificationChannel(ncJunk);
		final NotificationChannel ncHistorical = new NotificationChannel(NOTIFICATION_CHANNEL_HISTORICAL, getString(R.string.host_service_notification_channel_historical_name), NotificationManager.IMPORTANCE_LOW);
		nm.createNotificationChannel(ncHistorical);
		final NotificationChannel ncAuth = new NotificationChannel(NOTIFICATION_CHANNEL_AUTH, getString(R.string.host_service_notification_channel_auth_name), NotificationManager.IMPORTANCE_DEFAULT);
		nm.createNotificationChannel(ncAuth);
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!startedForeground) {
			startForeground(NOTIFICATION_ACTIVE, getOngoingNotification());
			startedForeground = true;
		}
		lastStartId = startId;

		switch (intent.getAction()) {
			case ACTION_ACCEPT_SLOT: {
				final String slot = intent.getStringExtra(EXTRA_SLOT);
				relayStart(slot);
				break;
			}
			case ACTION_START_SERVER: {
				directStart();
				break;
			}
			case ACTION_STOP_SERVER: {
				directFinish();
				break;
			}
			default:
				throw new IllegalArgumentException();
		}

		return START_NOT_STICKY;
	}

}

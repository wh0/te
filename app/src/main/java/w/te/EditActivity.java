package w.te;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import w.te.databinding.ActivityEditBinding;
import w.te.databinding.FindButtonsBinding;
import w.te.low.HookedEditText;
import w.te.rear.CommonInit;
import w.te.rear.Globals;
import w.te.rear.Repos;
import w.te.rear.Working;

public class EditActivity extends Activity {

	private static final long SAVE_DELAY = 10_000;
	private boolean doneLoading = false;
	private boolean needSave = false;
	private final AtomicInteger saveRequest = new AtomicInteger();
	private ActivityEditBinding binding;
	private String repoName;
	private String path;
	// Android's ICU-aware Matcher internally toString's its CharSequence,
	// so keep a cache around to avoid making too many copies while searching.
	private String findCache;

	private void maybeSave() {
		if (!needSave) return;
		Log.v("w", "EditActivity saving"); // %%%
		needSave = false;
		final String snapshot = binding.edit.getText().toString();
		final int request = saveRequest.incrementAndGet();
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			if (saveRequest.get() != request) return;
			try {
				Working.write(repoName, path, snapshot);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v("w", "EditActivity onCreate");
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
			Working.init();
		});
		super.onCreate(savedInstanceState);

		repoName = getIntent().getStringExtra("repo");
		path = getIntent().getStringExtra("path");
		setTitle(repoName + "/" + path);
		setTaskDescription(new ActivityManager.TaskDescription(repoName + "/" + path));
		binding = ActivityEditBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		binding.edit.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				findCache = null;
				if (doneLoading && !needSave) {
					Log.v("w", "EditActivity afterTextChanged setting needSave"); // %%%
					needSave = true;
					Globals.mainHandler.postDelayed(() -> {
						maybeSave();
					}, SAVE_DELAY);
				}
			}
		});
		binding.edit.hooks = new HookedEditText.Hooks() {
			@Override
			public boolean onKeyPreIme(int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					binding.edit.clearFocus();
					maybeSave();
				}
				return false;
			}
		};

		if (savedInstanceState == null) {
			Log.v("w", "EditActivity null savedInstanceState, loading from working"); // %%%
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				final CharSequence data;
				try {
					data = Working.read(repoName, path);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				runOnUiThread(() -> {
					binding.edit.setText(data);
					binding.edit.setEnabled(true);
					Log.v("w", "EditActivity loaded, enabling save"); // %%%
					doneLoading = true;
					if (data.length() == 0) binding.edit.requestFocus();
				});
			});
		} else {
			Log.v("w", "EditActivity savedInstanceState passed, won't load from working"); // %%%
		}
	}

	@Override
	protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		binding.edit.setEnabled(true);
		Log.v("w", "EditActivity onRestoreInstanceState, enabling save"); // %%%
		doneLoading = true;
	}

	private boolean handleFindMisc(String query) {
		switch (query) {
			case "/nopl":
				binding.edit.setImeOptions(binding.edit.getImeOptions() | EditorInfo.IME_FLAG_NO_PERSONALIZED_LEARNING);
				return true;
		}
		return false;
	}

	public void findPrev(String query) {
		if (findCache == null) findCache = binding.edit.getText().toString();
		final String regex = "^.*(" + Pattern.quote(query) + ")";
		final Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		final Matcher m = p.matcher(findCache);
		// find from selection to beginning
		m.region(0, binding.edit.getSelectionStart());
		if (m.find()) {
			binding.edit.setSelection(m.start(1), m.end(1));
			binding.edit.requestFocus();
			return;
		}
		// find from end to selection
		m.region(binding.edit.getSelectionEnd(), findCache.length());
		if (m.find()) {
			binding.edit.setSelection(m.start(1), m.end(1));
			binding.edit.requestFocus();
			return;
		}
		// matches intersecting selection will never be found
		Toast.makeText(this, R.string.find_not_found, Toast.LENGTH_SHORT).show();
	}

	public void findNext(String query) {
		if (findCache == null) findCache = binding.edit.getText().toString();
		// todo: all the trimmings like optional case sensitivity
		final String regex = Pattern.quote(query);
		final Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		final Matcher m = p.matcher(findCache);
		// find from selection to end
		m.region(binding.edit.getSelectionEnd(), findCache.length());
		if (m.find()) {
			binding.edit.setSelection(m.start(), m.end());
			binding.edit.requestFocus();
			return;
		}
		// find from beginning to selection
		m.region(0, binding.edit.getSelectionStart());
		if (m.find()) {
			binding.edit.setSelection(m.start(), m.end());
			binding.edit.requestFocus();
			return;
		}
		// matches intersecting selection will never be found
		Toast.makeText(this, R.string.find_not_found, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		final MenuInflater mi = getMenuInflater();
		mi.inflate(R.menu.edit_menu, menu);
		final MenuItem itemFind = menu.findItem(R.id.find);
		itemFind.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				findCache = null;
				return true;
			}
		});
		final SearchView actionViewFind = (SearchView) itemFind.getActionView();
		actionViewFind.setQueryHint(getString(R.string.hint_find));
		actionViewFind.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				if (handleFindMisc(query)) {
					Toast.makeText(EditActivity.this, R.string.find_misc_handled, Toast.LENGTH_SHORT).show();
					return true;
				}
				findNext(query);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
		final ActionBar ab = getActionBar();
		final LayoutInflater li = ab.getThemedContext().getSystemService(LayoutInflater.class);
		final FindButtonsBinding findButtons = FindButtonsBinding.inflate(li, ((LinearLayout) actionViewFind.getChildAt(0)), true);
		findButtons.findPrevBtn.setOnClickListener((v) -> {
			findPrev(actionViewFind.getQuery().toString());
		});
		findButtons.findNextBtn.setOnClickListener((v) -> {
			findNext(actionViewFind.getQuery().toString());
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.undo:
			case android.R.id.redo:
				return binding.edit.onTextContextMenuItem(item.getItemId());
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		maybeSave();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.v("w", "EditActivity onDestroy");
	}

}

package w.te;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import w.te.databinding.FileRowBinding;
import w.te.low.ShowUserException;
import w.te.rear.CommonInit;
import w.te.rear.Repos;
import w.te.rear.Working;

public class FileListActivity extends ListActivity {

	private static class Item implements Comparable<String> {

		public Working.Item working;
		public boolean dimmed;

		@Override
		public int compareTo(@NonNull String o) {
			return working.path.compareTo(o);
		}

	}

	private class Adapter extends BaseAdapter {

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Item getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Item item = items.get(position);
			final FileRowBinding binding;
			if (convertView == null) {
				binding = FileRowBinding.inflate(getLayoutInflater(), parent, false);
			} else {
				binding = FileRowBinding.bind(convertView);
			}
			if (item.dimmed) {
				binding.getRoot().setBackgroundResource(R.color.file_row_modified);
			} else {
				binding.getRoot().setBackground(null);
			}
			binding.path.setText(item.working.path);
			switch (item.working.status) {
				case Working.STATUS_CLEAN:
				case Working.STATUS_DELETED:
					if (item.working.lastCommit == Working.LAST_COMMIT_UNKNOWN) {
						binding.modified.setText(R.string.modified_committed);
					} else {
						binding.modified.setText(DateUtils.getRelativeTimeSpanString(item.working.lastCommit, startTime, DateUtils.MINUTE_IN_MILLIS));
					}
					binding.size.setText(Formatter.formatFileSize(FileListActivity.this, item.working.committedSize));
					break;
				case Working.STATUS_MODIFIED:
				case Working.STATUS_ADDED:
				case Working.STATUS_GONE:
					binding.modified.setText(DateUtils.getRelativeTimeSpanString(item.working.lastModified, startTime, DateUtils.MINUTE_IN_MILLIS));
					binding.size.setText(Formatter.formatFileSize(FileListActivity.this, item.working.modifiedSize));
					break;
			}
			switch (item.working.status) {
				case Working.STATUS_CLEAN:
					binding.status.setVisibility(View.INVISIBLE);
					break;
				case Working.STATUS_MODIFIED:
					binding.status.setVisibility(View.VISIBLE);
					binding.status.setText(R.string.status_modified);
					binding.status.setTextColor(getColor(R.color.status_modified));
					break;
				case Working.STATUS_ADDED:
					binding.status.setVisibility(View.VISIBLE);
					binding.status.setText(R.string.status_added);
					binding.status.setTextColor(getColor(R.color.status_added));
					break;
				case Working.STATUS_DELETED:
					binding.status.setVisibility(View.VISIBLE);
					binding.status.setText(R.string.status_deleted);
					binding.status.setTextColor(getColor(R.color.status_deleted));
					break;
				case Working.STATUS_GONE:
					binding.status.setVisibility(View.VISIBLE);
					binding.status.setText(R.string.status_gone);
					binding.status.setTextColor(getColor(R.color.status_gone));
					break;
			}
			return binding.getRoot();
		}

	}

	private String repoName;
	private long startTime;
	private List<Item> items;
	private Adapter adapter;
	private Working.Client client = new Working.Client() {

		@Override
		public void onList(List<Working.Item> list) {
			runOnUiThread(() -> {
				startTime = System.currentTimeMillis();
				items.clear();
				for (Working.Item wi : list) {
					final Item item = new Item();
					item.working = wi;
					items.add(item);
				}
				adapter.notifyDataSetChanged();
			});
		}

		@Override
		public void onLastCommit(String path, long lastCommit) {
			runOnUiThread(() -> {
				final Item item = getItemByPath(path);
				if (item != null) {
					item.working.lastCommit = lastCommit;
					adapter.notifyDataSetChanged();
				}
			});
		}

		@Override
		public void onGiveUp() {
			// nothing for now
		}

		@Override
		public void onGone(String path) {
			runOnUiThread(() -> {
				final Item item = getItemByPath(path);
				if (item != null) {
					item.dimmed = true;
					item.working.status = Working.STATUS_GONE;
					adapter.notifyDataSetChanged();
				}
			});
		}

		@Override
		public void onDeleted(String path) {
			runOnUiThread(() -> {
				final Item item = getItemByPath(path);
				if (item != null) {
					item.dimmed = true;
					item.working.status = Working.STATUS_DELETED;
					adapter.notifyDataSetChanged();
				}
			});
		}

		@Override
		public void onReset(String path) {
			runOnUiThread(() -> {
				final Item item = getItemByPath(path);
				if (item != null) {
					item.dimmed = true;
					item.working.status = Working.STATUS_CLEAN;
					adapter.notifyDataSetChanged();
				}
			});
		}

		@Override
		public boolean isCurrent(int cookie) {
			return refreshRequest.get() == cookie;
		}

	};

	private final AtomicInteger refreshRequest = new AtomicInteger();

	private void refreshInternal(int request) {
		if (refreshRequest.get() != request) return;
		try {
			Working.list(repoName, client, request);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void refresh() {
		final int request = refreshRequest.incrementAndGet();
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			refreshInternal(request);
		});
	}

	private void commit() {
		final int request = refreshRequest.incrementAndGet();
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				Working.commit(repoName);
			} catch (IOException e) {
				throw new RuntimeException(e);
			} catch (ShowUserException e) {
				runOnUiThread(() -> {
					e.showToast(this);
				});
				return;
			}
			refreshInternal(request);
		});
	}

	private Item getItemByPath(String path) {
		final int index = Collections.binarySearch(items, path);
		if (index < 0) {
			return null;
		} else {
			return items.get(index);
		}
	}

	private void delete(String path) {
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				Working.delete(repoName, path, client);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		});
	}

	private void promptDelete(String path) {
		new AlertDialog.Builder(this)
				.setTitle(R.string.dialog_delete_title)
				.setMessage(R.string.dialog_delete_message)
				.setPositiveButton(R.string.dialog_delete_positive, (dialog, which) -> {
					delete(path);
				})
				.setNegativeButton(android.R.string.cancel, null)
				.create()
				.show();
	}

	private void reset(String path) {
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				Working.reset(repoName, path, client);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		});
	}

	private void promptReset(String path) {
		new AlertDialog.Builder(this)
				.setTitle(R.string.dialog_reset_title)
				.setMessage(R.string.dialog_reset_message)
				.setPositiveButton(R.string.dialog_reset_positive, (dialog, which) -> {
					reset(path);
				})
				.setNegativeButton(android.R.string.cancel, null)
				.create()
				.show();
	}

	private void diff(String path) {
		final Intent intent = new Intent(this, DiffActivity.class)
				.putExtra("repo", repoName)
				.putExtra("path", path);
		startActivity(intent);
	}

	private void open(String path) {
		final Intent intent = new Intent(this, EditActivity.class)
				.setData(Uri.fromParts("x", repoName + "/" + path, null))
				.putExtra("repo", repoName)
				.putExtra("path", path);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
			Working.init();
		});
		super.onCreate(savedInstanceState);

		repoName = getIntent().getStringExtra("repo");
		setTitle(repoName);
		registerForContextMenu(getListView());
		items = new ArrayList<>();
		adapter = new Adapter();
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		refresh();
	}

	@Override
	protected void onStop() {
		super.onStop();
		refreshRequest.incrementAndGet();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.file_list_menu, menu);
		final MenuItem itemNewFile = menu.findItem(R.id.new_file);
		final SearchView actionViewNewFile = (SearchView) itemNewFile.getActionView();
		actionViewNewFile.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
		actionViewNewFile.setImeOptions(EditorInfo.IME_ACTION_GO);
		actionViewNewFile.setQueryHint(getString(R.string.hint_path));
		actionViewNewFile.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				itemNewFile.collapseActionView();
				open(query);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (menuItem.getItemId() == R.id.refresh) {
			refresh();
			return true;
		} else if (menuItem.getItemId() == R.id.commit) {
			commit();
			return true;
		} else if (menuItem.getItemId() == R.id.backups) {
			final Intent intent = new Intent(this, BackupActivity.class)
					.putExtra("repo", repoName);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		open(items.get(position).working.path);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.file_list_context_menu, menu);
		final int position = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
		final Item item = items.get(position);
		switch (item.working.status) {
			case Working.STATUS_CLEAN:
			case Working.STATUS_MODIFIED:
			case Working.STATUS_ADDED:
				menu.findItem(R.id.delete).setVisible(true);
		}
		switch (item.working.status) {
			case Working.STATUS_MODIFIED:
			case Working.STATUS_DELETED:
				menu.findItem(R.id.reset).setVisible(true);
		}
		switch (item.working.status) {
			case Working.STATUS_MODIFIED:
				menu.findItem(R.id.diff).setVisible(true);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
		final int position = ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position;
		final Item item = items.get(position);
		final String path = item.working.path;
		if (menuItem.getItemId() == R.id.delete) {
			switch (item.working.status) {
				case Working.STATUS_MODIFIED:
				case Working.STATUS_ADDED:
					promptDelete(path);
					break;
				default:
					delete(path);
			}
			return true;
		} else if (menuItem.getItemId() == R.id.reset) {
			switch (item.working.status) {
				case Working.STATUS_MODIFIED:
				case Working.STATUS_ADDED:
					promptReset(path);
					break;
				default:
					reset(path);
			}
			return true;
		} else if (menuItem.getItemId() == R.id.diff) {
			diff(path);
			return true;
		}
		return false;
	}

}

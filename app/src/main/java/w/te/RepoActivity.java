package w.te;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import w.te.databinding.RepoRowBinding;
import w.te.low.ShowUserException;
import w.te.rear.CommonInit;
import w.te.rear.Repos;

public class RepoActivity extends ListActivity {

	private List<String> repos;
	private Adapter adapter;

	private class Adapter extends BaseAdapter {

		@Override
		public int getCount() {
			return repos.size();
		}

		@Override
		public String getItem(int position) {
			return repos.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final String name = getItem(position);
			final RepoRowBinding binding;
			if (convertView == null) {
				binding = RepoRowBinding.inflate(getLayoutInflater(), parent, false);
			} else {
				binding = RepoRowBinding.bind(convertView);
			}
			binding.name.setText(name);
			return binding.getRoot();
		}

	}

	private final AtomicInteger refreshRequest = new AtomicInteger();
	private void refreshInternal(int request) {
		if (refreshRequest.get() != request) return;
		final List<String> result = Repos.list();
		runOnUiThread(() -> {
			repos = result;
			adapter.notifyDataSetChanged();
		});
	}

	private void refresh() {
		final int request = refreshRequest.incrementAndGet();
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			refreshInternal(request);
		});
	}

	private void delete(String name) {
		final int request = refreshRequest.incrementAndGet();
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			try {
				Repos.delete(name);
			} catch (IOException e) {
				throw new RuntimeException(e);
			} catch (ShowUserException e) {
				runOnUiThread(() -> {
					e.showToast(this);
				});
				return;
			}
			refreshInternal(request);
		});
	}

	private void promptDelete(String name) {
		new AlertDialog.Builder(this)
				.setTitle(R.string.dialog_delete_repo_title)
				.setMessage(R.string.dialog_delete_repo_message)
				.setPositiveButton(R.string.dialog_delete_repo_positive, (dialog, which) -> {
					delete(name);
				})
				.setNegativeButton(android.R.string.cancel, null)
				.create()
				.show();
	}

	private void open(String name) {
		final Intent intent = new Intent(this, FileListActivity.class)
				.putExtra("repo", name);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
		});
		super.onCreate(savedInstanceState);
		registerForContextMenu(getListView());
		repos = Collections.emptyList();
		adapter = new Adapter();
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		refresh();
	}

	@Override
	protected void onStop() {
		super.onStop();
		refreshRequest.incrementAndGet();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.repo_menu, menu);
		final MenuItem itemNewRepo = menu.findItem(R.id.new_repo);
		final SearchView actionViewNewRepo = (SearchView) itemNewRepo.getActionView();
		actionViewNewRepo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
		actionViewNewRepo.setImeOptions(EditorInfo.IME_ACTION_GO);
		actionViewNewRepo.setQueryHint(getString(R.string.hint_name));
		actionViewNewRepo.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				final int request = refreshRequest.incrementAndGet();
				AsyncTask.SERIAL_EXECUTOR.execute(() -> {
					try {
						Repos.create(query);
					} catch (IOException e) {
						throw new RuntimeException(e);
					} catch (ShowUserException e) {
						runOnUiThread(() -> {
							e.showToast(RepoActivity.this);
						});
						return;
					}
					refreshInternal(request);
					runOnUiThread(() -> {
						itemNewRepo.collapseActionView();
						open(query);
					});
				});
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (menuItem.getItemId() == R.id.refresh) {
			refresh();
			return true;
		} else if (menuItem.getItemId() == R.id.host_settings) {
			final Intent intent = new Intent(this, HostSettingsActivity.class);
			startActivity(intent);
			return true;
		} else if (menuItem.getItemId() == R.id.commit_settings) {
			final Intent intent = new Intent(this, CommitSettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		open(repos.get(position));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.repo_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
		final int position = ((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position;
		final String name = repos.get(position);
		if (menuItem.getItemId() == R.id.delete) {
			promptDelete(name);
			return true;
		}
		return false;
	}

}

package w.te;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import androidx.annotation.Nullable;

import w.te.rear.CommitSettings;
import w.te.rear.CommonInit;

public class CommitSettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.replace(android.R.id.content, new Frag())
					.commit();
		}
	}

	public static class Frag extends PreferenceFragment {
		@Override
		public void onCreate(@Nullable Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			CommitSettings.configurePreferenceManager(getPreferenceManager());
			addPreferencesFromResource(R.xml.settings_commit);
		}
	}

}

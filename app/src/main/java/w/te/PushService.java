package w.te;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import w.te.rear.CommonInit;
import w.te.rear.Host;
import w.te.rear.HostSettings;

// todo: move closed source firebase sdk to separate app
public class PushService extends FirebaseMessagingService {

	@Override
	public void onCreate() {
		CommonInit.initFront(getApplicationContext());
		super.onCreate();
	}

	@Override
	public void onMessageReceived(RemoteMessage message) {
		final String slot = message.getData().get("slot");
		Log.v("w", "PushService onMessageReceived " + slot); // %%%
		if (!Host.RelayHost.SLOT_PATTERN.matcher(slot).matches()) {
			Log.e("w", "accept_slot: malformed slot " + slot);
			return;
		}
		final SharedPreferences prefs = HostSettings.getSharedPreferences();
		if (!prefs.getBoolean("relay_enable", false)) {
			Log.e("w", "accept_slot: received message while relayed connections are not enabled");
			return;
		}
		final Intent intent = new Intent(this, HostService.class)
				.setAction(HostService.ACTION_ACCEPT_SLOT)
				.putExtra(HostService.EXTRA_SLOT, slot);
		startForegroundService(intent);
	}

	// FCM 18+ no longer informs us when the token has changed, unless you opt in to having an
	// automatic default token. If you do any of the things mentioned here, then re-register
	// manually:
	// https://firebase.google.com/docs/cloud-messaging/android/client#sample-register

}

package w.te;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.text.InputFilter;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Observer;
import java.util.concurrent.ExecutionException;

import w.te.low.AuthorizedKeysPreference;
import w.te.low.HostnameFilter;
import w.te.low.ShowUserException;
import w.te.low.SummaryEditTextPreference;
import w.te.rear.CommonInit;
import w.te.rear.Globals;
import w.te.rear.Host;
import w.te.rear.HostSettings;
import w.te.rear.Repos;

public class HostSettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
			Host.init();
		});
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.replace(android.R.id.content, new Frag())
					.commit();
		}
	}

	public static class Frag extends PreferenceFragment {
		private AuthorizedKeysPreference authAuthorizedKeys;
		private final Observer authorizedKeysObserver = (o, arg) -> {
			final String authorizedKeys = getPreferenceManager().getSharedPreferences().getString("auth_authorized_key", null);
			authAuthorizedKeys.setPersistent(false);
			authAuthorizedKeys.setText(authorizedKeys);
			authAuthorizedKeys.setPersistent(true);
		};

		public static final int REQUEST_CODE_NOTIFICATIONS = 1;
		private Preference notificationsPermission;
		private void refreshNotificationsPermission() {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) return;
			int permissionResult = getContext().checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS);
			switch (permissionResult) {
				case PackageManager.PERMISSION_DENIED:
					notificationsPermission.setSummary(R.string.notifications_permission_denied);
					break;
				case PackageManager.PERMISSION_GRANTED:
					notificationsPermission.setSummary(R.string.notifications_permission_granted);
					break;
				default:
					throw new IllegalStateException();
			}
		}

		private SwitchPreference directEnable;
		private final Observer directStateObserver = (o, arg) -> {
			syncDirectEnable();
			if (HostService.directState == HostService.STATE_STARTED) {
				refreshDirectIps();
			}
		};

		private Preference directIps;
		private Map<Network, LinkProperties> networks = null;
		private final ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
			@Override
			public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
				networks.put(network, linkProperties);
				refreshDirectIps();
			}

			@Override
			public void onLost(Network network) {
				networks.remove(network);
				refreshDirectIps();
			}
		};

		private void syncDirectEnable() {
			switch (HostService.directState) {
				case HostService.STATE_STOPPED:
					directEnable.setChecked(false);
					directEnable.setEnabled(true);
					break;
				case HostService.STATE_STARTING:
					directEnable.setChecked(true);
					directEnable.setEnabled(false);
					break;
				case HostService.STATE_STARTED:
					directEnable.setChecked(true);
					directEnable.setEnabled(true);
					break;
				case HostService.STATE_STOPPING:
					directEnable.setChecked(false);
					directEnable.setEnabled(false);
					break;
			}
		}

		private String getInetAddressesMaybe() {
			if (networks == null) return null;
			final StringBuilder sb = new StringBuilder();
			boolean any = false;
			for (LinkProperties lp : networks.values()) {
				for (LinkAddress la : lp.getLinkAddresses()) {
					final InetAddress ia = la.getAddress();
					if (ia.isAnyLocalAddress()) continue;
					if (ia.isLinkLocalAddress()) continue;
					if (ia.isLoopbackAddress()) continue;
					if (ia.isMulticastAddress()) continue;
					if (any) {
						sb.append("\n");
					} else {
						any = true;
					}
					sb.append(lp.getInterfaceName()).append(" ").append(ia.getHostAddress()).append("/").append(la.getPrefixLength());
				}
			}
			if (!any) return null;
			return sb.toString();
		}

		private void refreshDirectIps() {
			final String addrs = getInetAddressesMaybe();
			if (addrs == null) {
				directIps.setSummary(R.string.direct_ips_unavailable);
			} else {
				directIps.setSummary(addrs);
			}
			directIps.setEnabled(true);
		}

		private Preference relayLastRegistered;

		private void register(SharedPreferences prefs) {
			relayLastRegistered.setSummary(R.string.last_registered_registering);
			AsyncTask.THREAD_POOL_EXECUTOR.execute(() -> {
				try {
					HostSettings.registerToken(prefs);
				} catch (IOException | ExecutionException | InterruptedException e) {
					Log.e("w", "registerToken", e);
					getActivity().runOnUiThread(() -> {
						relayLastRegistered.setSummary(getResources().getString(R.string.error_relay_register_token_io, e));
					});
					return;
				} catch (ShowUserException e) {
					Log.e("w", "registerToken", e);
					getActivity().runOnUiThread(() -> {
						e.setSummary(relayLastRegistered);
					});
					return;
				}
				getActivity().runOnUiThread(() -> {
					relayLastRegistered.setSummary(R.string.last_registered_now);
				});
			});
		}

		private void unregister(SharedPreferences prefs) {
			relayLastRegistered.setSummary(R.string.last_registered_unregistering);
			AsyncTask.THREAD_POOL_EXECUTOR.execute(() -> {
				try {
					HostSettings.unregister(prefs);
				} catch (IOException | ExecutionException | InterruptedException e) {
					Log.e("w", "unregister", e);
					getActivity().runOnUiThread(() -> {
						relayLastRegistered.setSummary(getResources().getString(R.string.error_relay_unregister_token_io, e));
					});
					return;
				}
				getActivity().runOnUiThread(() -> {
					relayLastRegistered.setSummary(R.string.last_registered_never);
				});
			});
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			HostSettings.configurePreferenceManager(getPreferenceManager());
			addPreferencesFromResource(R.xml.settings_host);

			final Preference authHostKeyFingerprint = findPreference("auth_host_key_fingerprint");
			authHostKeyFingerprint.setEnabled(false);
			AsyncTask.SERIAL_EXECUTOR.execute(() -> {
				final String hostKeyFingerprint;
				try {
					hostKeyFingerprint = Host.getHostKeyFingerprint();
				} catch (IOException | GeneralSecurityException e) {
					throw new RuntimeException(e); // %%%
				}
				getActivity().runOnUiThread(() -> {
					authHostKeyFingerprint.setSummary(hostKeyFingerprint);
					authHostKeyFingerprint.setEnabled(true);
				});
			});
			authAuthorizedKeys = (AuthorizedKeysPreference) findPreference("auth_authorized_key");

			notificationsPermission = findPreference("notifications_permission");
			notificationsPermission.setOnPreferenceClickListener((preference) -> {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
					int permission = getContext().checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS);
					boolean wantRationale = shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS);
					if (permission == PackageManager.PERMISSION_DENIED && wantRationale) {
						requestPermissions(new String[]{Manifest.permission.POST_NOTIFICATIONS}, REQUEST_CODE_NOTIFICATIONS);
						return true;
					}
				}
				Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
						.putExtra(Settings.EXTRA_APP_PACKAGE, getContext().getPackageName());
				startActivity(intent);
				return true;
			});

			directEnable = (SwitchPreference) findPreference("direct_enable");
			directEnable.setOnPreferenceChangeListener((preference, newValue) -> {
				if ((boolean) newValue) {
					directEnable.setEnabled(false);
					final Intent intent = new Intent(getContext(), HostService.class)
							.setAction(HostService.ACTION_START_SERVER);
					getContext().startForegroundService(intent);
				} else {
					directEnable.setEnabled(false);
					final Intent intent = new Intent(getContext(), HostService.class)
							.setAction(HostService.ACTION_STOP_SERVER);
					getContext().startForegroundService(intent);
				}
				return true;
			});

			directIps = findPreference("direct_ips");
			directIps.setEnabled(false);

			findPreference("relay_enable").setOnPreferenceChangeListener((preference, newValue) -> {
				final SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
				if ((boolean) newValue) {
					register(prefs);
				} else {
					unregister(prefs);
				}
				return true;
			});
			final Preference.OnPreferenceChangeListener invalidateRegistrationListener = (preference, newValue) -> {
				final SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
				HostSettings.resetTokenRegistration(prefs);
				relayLastRegistered.setSummary(R.string.last_registered_invalidated);
				return true;
			};
			findPreference("relay_custom_sender_id").setOnPreferenceChangeListener(invalidateRegistrationListener);
			final SummaryEditTextPreference relayCustomHost = (SummaryEditTextPreference) findPreference("relay_custom_host");
			relayCustomHost.getEditText().setFilters(new InputFilter[]{new HostnameFilter()});
			relayCustomHost.setOnPreferenceChangeListener(invalidateRegistrationListener);
			findPreference("relay_custom_host").setOnPreferenceChangeListener(invalidateRegistrationListener);
			findPreference("relay_nickname").setOnPreferenceChangeListener(invalidateRegistrationListener);
			// changing the knock doesn't affect registration
			relayLastRegistered = findPreference("relay_last_registered");
			relayLastRegistered.setOnPreferenceClickListener((preference) -> {
				final SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
				register(prefs);
				return true;
			});
		}

		@Override
		public void onStart() {
			super.onStart();

			HostSettings.authorizedKeysObservable.addObserver(authorizedKeysObserver);

			refreshNotificationsPermission();

			syncDirectEnable();
			HostService.directStateObservable.addObserver(directStateObserver);

			networks = new LinkedHashMap<>();
			getContext().getSystemService(ConnectivityManager.class).registerNetworkCallback(new NetworkRequest.Builder().build(), networkCallback, Globals.mainHandler);
		}

		@Override
		public void onStop() {
			super.onStop();

			HostSettings.authorizedKeysObservable.deleteObserver(authorizedKeysObserver);

			HostService.directStateObservable.deleteObserver(directStateObserver);

			getContext().getSystemService(ConnectivityManager.class).unregisterNetworkCallback(networkCallback);
			networks = null;
		}

		@Override
		public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
			switch (requestCode) {
				case REQUEST_CODE_NOTIFICATIONS:
					refreshNotificationsPermission();
			}
		}
	}

}

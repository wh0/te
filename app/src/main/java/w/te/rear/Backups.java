package w.te.rear;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.operator.bc.BcPBEKeyEncryptionMethodGenerator;
import org.bouncycastle.openpgp.operator.bc.BcPGPDataEncryptorBuilder;
import org.bouncycastle.util.Strings;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.BundleWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import w.te.low.KeepableTempFile;

public class Backups {

	private static final char[] PASSPHRASE_DISABLE_ENCRYPTION = "don't encrypt".toCharArray();

	public static String getBackupUri(String repoName) {
		return "content://" + Globals.applicationContext.getPackageName() + ".BackupActivity.Provider/" + repoName;
	}

	public static class Bundle {
		public Path diamond;
		public String fromId;
		public String toId;
	}

	@Nullable
	private static Bundle queryInternal(SQLiteDatabase db, String uri) {
		final Bundle bundle;
		try (Cursor c = db.query("backups", new String[]{"name", "from_id", "to_id"},
				"uri = ?", new String[]{uri},
				null, null, null)) {
			if (c.moveToFirst()) {
				bundle = new Bundle();
				final String name = c.getString(c.getColumnIndexOrThrow("name"));
				bundle.diamond = Globals.applicationContext.getCacheDir().toPath().resolve(name);
				bundle.fromId = c.getString(c.getColumnIndexOrThrow("from_id"));
				bundle.toId = c.getString(c.getColumnIndexOrThrow("to_id"));
			} else {
				bundle = null;
			}
		}
		return bundle;
	}

	public static Bundle query(String uri) {
		final Bundle bundle;
		try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				bundle = queryInternal(db, uri);
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
		return bundle;
	}

	public interface Client {
		void onLastId(String lastId);
		void onBackupReady(Bundle b);
		void onProgressUpdateTask(String title, int work, int totalWork);
		void onProgressEndTask();
		void onProgressComplete();
	}

	public static void check(String repoName, String uri, Client client) {
		try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				final String lastId;
				try (Cursor c = db.query("last_backup", new String[]{"last_id"},
						"repo = ?", new String[]{repoName},
						null, null, null)) {
					if (c.moveToFirst()) {
						lastId = c.getString(c.getColumnIndexOrThrow("last_id"));
					} else {
						lastId = null;
					}
				}
				if (lastId != null) {
					client.onLastId(lastId);
				}
				final Bundle bundle = queryInternal(db, uri);
				if (bundle != null) {
					client.onBackupReady(bundle);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	private static class Progress implements ProgressMonitor {

		private final Client client;
		private String taskTitle;
		private int taskWork;
		private int taskTotalWork;

		private Progress(Client client) {
			this.client = client;
		}

		@Override
		public void start(int totalTasks) {
			// lol nothing
		}

		@Override
		public void beginTask(String title, int totalWork) {
			taskTitle = title;
			taskWork = 0;
			taskTotalWork = totalWork;
			client.onProgressUpdateTask(taskTitle, taskWork, taskTotalWork);
		}

		@Override
		public void update(int completed) {
			taskWork += completed;
			client.onProgressUpdateTask(taskTitle, taskWork, taskTotalWork);
		}

		@Override
		public void endTask() {
			client.onProgressEndTask();
		}

		@Override
		public boolean isCancelled() {
			return false;
		}

	}

	private static void dump(String repoName, OutputStream dst, String have, Bundle bundle, Client client) throws IOException {
		try (Repository repo = Repos.getRepo(repoName)) {
			final BundleWriter bw = new BundleWriter(repo);
			final Ref headRef = repo.exactRef(Constants.HEAD);
			final ObjectId head = headRef.getObjectId();
			if (head == null) throw new IllegalStateException();
			bundle.toId = head.name();
			bw.include(headRef);
			bw.include(headRef.getTarget());
			if (have != null) {
				try (RevWalk rw = new RevWalk(repo)) {
					final RevCommit haveCommit = rw.parseCommit(ObjectId.fromString(have));
					bundle.fromId = haveCommit.name();
					bw.assume(haveCommit);
				}
			}
			bw.writeBundle(new Progress(client), dst);
			client.onProgressComplete();
		}
	}

	private static final String DUMMY_NAME = "";

	private static OutputStream transduce(OutputStream dst, char[] passphrase) throws IOException, PGPException {
		final PGPEncryptedDataGenerator edg = new PGPEncryptedDataGenerator(
				new BcPGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.AES_256)
						.setWithIntegrityPacket(true)
		);
		edg.addMethod(new BcPBEKeyEncryptionMethodGenerator(passphrase));
		final OutputStream edos = edg.open(dst, new byte[8192]);
		final PGPLiteralDataGenerator ldg = new PGPLiteralDataGenerator();
		final OutputStream ldos = ldg.open(edos, PGPLiteralDataGenerator.BINARY, DUMMY_NAME, PGPLiteralDataGenerator.NOW, new byte[8192]);
		return new OutputStream() {

			@Override
			public void write(int b) throws IOException {
				ldos.write(b);
			}

			@Override
			public void write(@NonNull byte[] b, int off, int len) throws IOException {
				ldos.write(b, off, len);
			}

			@Override
			public void flush() throws IOException {
				ldos.flush();
			}

			@Override
			public void close() throws IOException {
				ldos.close();
				edos.close();
			}

		};
	}

	private static void save(SQLiteDatabase db, String uri, KeepableTempFile k, Bundle bundle) {
		final ContentValues cv = new ContentValues(4);
		cv.put("uri", uri);
		cv.put("name", k.path.getFileName().toString());
		cv.put("from_id", bundle.fromId);
		cv.put("to_id", bundle.toId);
		db.insertOrThrow("backups", null, cv);
		bundle.diamond = k.keep();
	}

	private static void createInternalEncrypted(SQLiteDatabase db, String uri, String repoName, char[] passphrase, String have, Bundle bundle, Client client) throws IOException, PGPException {
		try (KeepableTempFile k = new KeepableTempFile(Globals.applicationContext.getCacheDir().toPath(), repoName + "-", ".bundle.gpg")) {
			try (
					final OutputStream kos = k.getOutputStream();
					final OutputStream dst = transduce(kos, passphrase);
			) {
				dump(repoName, dst, have, bundle, client);
			}
			save(db, uri, k, bundle);
		}
	}

	private static void createInternalClear(SQLiteDatabase db, String uri, String repoName, String have, Bundle bundle, Client client) throws IOException, PGPException {
		try (KeepableTempFile k = new KeepableTempFile(Globals.applicationContext.getCacheDir().toPath(), repoName + "-", ".bundle")) {
			try (OutputStream dst = k.getOutputStream()) {
				dump(repoName, dst, have, bundle, client);
			}
			save(db, uri, k, bundle);
		}
	}

	public static void create(String repoName, String uri, String have, char[] passphrase, Client client) throws IOException, PGPException {
		final Bundle bundle = new Bundle();
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				if (Arrays.equals(passphrase, PASSPHRASE_DISABLE_ENCRYPTION)) {
					createInternalClear(db, uri, repoName, have, bundle, client);
				} else {
					createInternalEncrypted(db, uri, repoName, passphrase, have, bundle, client);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
		client.onBackupReady(bundle);
	}

	public static void discard(String uri, Path diamond) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				db.delete("backups", "uri = ?", new String[]{uri});
				Files.delete(diamond);
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	public static void finish(String repoName, String uri, Bundle bundle) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				db.delete("backups", "uri = ?", new String[]{uri});
				final ContentValues cv = new ContentValues(2);
				cv.put("repo", repoName);
				cv.put("last_id", bundle.toId);
				db.replaceOrThrow("last_backup", null, cv);
				Files.delete(bundle.diamond);
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	// unused fixed-length transduce below

	private static long calcNewHeaderLength(long len) {
		if (len < 192) return 2;
		if (len < 8384) return 3;
		return 6;
	}

	private static long calcPlaintextPacketLength(String name, long length) {
		final long nameLen = Strings.toUTF8ByteArray(name).length;
		final long ptLen = 1 + 1 + nameLen + 4 + length;
		return calcNewHeaderLength(ptLen) + ptLen;
	}

	private static OutputStream transduce(OutputStream dst, char[] passphrase, long length) throws IOException, PGPException {
		final PGPEncryptedDataGenerator edg = new PGPEncryptedDataGenerator(
				new BcPGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.AES_256)
						.setWithIntegrityPacket(true)
		);
		edg.addMethod(new BcPBEKeyEncryptionMethodGenerator(passphrase));
		final OutputStream edos = edg.open(dst, calcPlaintextPacketLength(DUMMY_NAME, length));
		final PGPLiteralDataGenerator ldg = new PGPLiteralDataGenerator();
		final OutputStream ldos = ldg.open(edos, PGPLiteralDataGenerator.BINARY, DUMMY_NAME, length, PGPLiteralDataGenerator.NOW);
		return new OutputStream() {

			@Override
			public void write(int b) throws IOException {
				ldos.write(b);
			}

			@Override
			public void write(@NonNull byte[] b, int off, int len) throws IOException {
				ldos.write(b, off, len);
			}

			@Override
			public void flush() throws IOException {
				ldos.flush();
			}

			@Override
			public void close() throws IOException {
				ldos.close();
				edos.close();
			}

		};
	}

}

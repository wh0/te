package w.te.rear;

import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.apache.sshd.common.AttributeRepository;
import org.apache.sshd.common.config.keys.AuthorizedKeyEntry;
import org.apache.sshd.common.config.keys.KeyUtils;
import org.apache.sshd.common.helpers.HelpersAccess;
import org.apache.sshd.common.io.IoConnector;
import org.apache.sshd.common.io.IoSession;
import org.apache.sshd.common.keyprovider.KeyPairProvider;
import org.apache.sshd.common.util.OsUtils;
import org.apache.sshd.common.util.buffer.ByteArrayBuffer;
import org.apache.sshd.common.util.security.SecurityUtils;
import org.apache.sshd.common.util.security.bouncycastle.BouncyCastleSecurityProviderRegistrar;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.channel.ChannelSession;
import org.apache.sshd.server.command.Command;
import org.apache.sshd.server.keyprovider.AbstractGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.session.SessionFactory;
import org.apache.sshd.server.shell.InvertedShellWrapper;
import org.apache.sshd.server.shell.ProcessShell;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.ReceiveCommand;
import org.eclipse.jgit.transport.ReceivePack;
import org.eclipse.jgit.transport.UploadPack;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.net.ProtocolException;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import w.te.low.AndroidOpenSSLSecurityProviderRegistrar;
import w.te.low.ForkptyShell;
import w.te.low.SessionAuthorizedKeysAuthenticator;

public class Host {

	// todo: maybe split this into host and relay

	private static final Pattern HEADER_GOOD = Pattern.compile("^X-Te-Good: yes\r\n", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNIX_LINES);
	private static final Pattern COMMAND_PATTERN = Pattern.compile("(git-receive-pack|git-upload-pack) '([^-'][^']*)'");

	private static boolean initialized;
	public static void init() {
		if (initialized) return;

		try {
			OsUtils.setAndroid(true);

			System.setProperty(SecurityUtils.SECURITY_PROVIDER_REGISTRARS, AndroidOpenSSLSecurityProviderRegistrar.class.getName() + "," + BouncyCastleSecurityProviderRegistrar.class.getName());

			final Path sshDir = Globals.applicationContext.getFilesDir().toPath().resolve(".ssh");
			Files.createDirectories(sshDir);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		initialized = true;
	}

	private static KeyPairProvider getKeyPairProvider() {
		final AbstractGeneratorHostKeyProvider k = SecurityUtils.createGeneratorHostKeyProvider(Globals.applicationContext.getFilesDir().toPath().resolve(".ssh/ssh_host_rsa_key"));
		k.setAlgorithm(KeyUtils.RSA_ALGORITHM);
		return k;
	}

	public static String getHostKeyFingerprint() throws IOException, GeneralSecurityException {
		final KeyPairProvider kpp = getKeyPairProvider();
		final KeyPair kp = kpp.loadKey(null, KeyPairProvider.SSH_RSA);
		final PublicKey k = kp.getPublic();
		return KeyUtils.getFingerPrint(k);
	}

	private static abstract class CommandAdapter implements Command, Runnable {

		private InputStream in;
		private OutputStream out;
		private OutputStream err;
		private ExitCallback exitCallback;
		private Thread thread;

		public abstract void run(InputStream in, OutputStream out, OutputStream err) throws Exception;

		@Override public void setOutputStream(OutputStream s) { out = s; }
		@Override public void setInputStream(InputStream s) { in = s; }
		@Override public void setErrorStream(OutputStream s) { err = s; }
		@Override public void setExitCallback(ExitCallback cb) { exitCallback = cb; }

		@Override
		public void run() {
			try {
				run(in, out, err);
			} catch (Exception e) {
				try (PrintStream ps = new PrintStream(err)) {
					e.printStackTrace(ps);
				}
				exitCallback.onExit(1);
				return;
			}
			exitCallback.onExit(0);
		}

		@Override
		public void start(ChannelSession channel, Environment env) throws IOException {
			thread = new Thread(this, "command-thread");
			thread.start();
		}

		@Override
		public void destroy(ChannelSession channel) throws Exception {
			thread.interrupt();
		}

	}

	public interface Client {
		void onDirectSessionCreated(long id, SocketAddress remoteAddress);
		void onDirectSessionClosed(long id);
		void onRelayConnectSuccess(String slot, long id);
		void onRelayConnectFail(String slot);
		void onRelaySessionCreated(long id);
		void onRelaySessionClosed(long id);
		void onAuthFail(PublicKey publicKey);
		void onPrePush(String repoName);
		void onPush(String authorizedKeyComment, String repoName);
		void onPreShell(String authorizedKeyComment);
	}

	private static SshServer configureServer(SharedPreferences prefs, Client client) {
		final SshServer s = SshServer.setUpDefaultServer();
		s.setKeyPairProvider(getKeyPairProvider());
		s.setPublickeyAuthenticator(new SessionAuthorizedKeysAuthenticator() {
			@Override
			protected List<AuthorizedKeyEntry> getAuthorizedKeys() throws IOException {
				final String authorizedKeysRaw = prefs.getString("auth_authorized_key", null);
				if (authorizedKeysRaw == null) return Collections.emptyList();
				return AuthorizedKeyEntry.readAuthorizedKeys(new StringReader(authorizedKeysRaw), true);
			}

			@Override
			protected void onFail(String username, PublicKey key, ServerSession session) {
				client.onAuthFail(key);
			}
		});
		s.setCommandFactory((channel, command) -> {
			final Matcher m = COMMAND_PATTERN.matcher(command);
			if (m.matches()) {
				final String repoName = m.group(2);
				switch (m.group(1)) {
					case "git-receive-pack":
						return new CommandAdapter() {
							@Override
							public void run(InputStream in, OutputStream out, OutputStream err) throws Exception {
								client.onPrePush(repoName);
								try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
									db.beginTransaction();
									final boolean isClean = Working.isClean(db, repoName);
									try {
										try (Repository repo = Repos.getRepo(repoName)) {
											final ReceivePack rp = new ReceivePack(repo);
											if (!isClean) {
												final Ref headLeaf = repo.exactRef(Constants.HEAD).getLeaf();
												rp.setPreReceiveHook((rp1, commands) -> {
													for (final ReceiveCommand command : commands) {
														if (command.getRef() == headLeaf) {
															command.setResult(ReceiveCommand.Result.REJECTED_CURRENT_BRANCH);
														}
													}
												});
											}
											rp.setPostReceiveHook((rp1, commands) -> {
												if (commands.isEmpty()) return;
												// todo: refresh stuff?
												client.onPush(channel.getSession().getAttribute(SessionAuthorizedKeysAuthenticator.AUTHORIZED_KEY).getComment(), repoName);
											});
											rp.receive(in, out, err);
										}
									} finally {
										db.endTransaction();
									}
								}
							}
						};
					case "git-upload-pack":
						return new CommandAdapter() {
							@Override
							public void run(InputStream in, OutputStream out, OutputStream err) throws IOException {
								try (Repository repo = Repos.getRepo(repoName)) {
									new UploadPack(repo).upload(in, out, err);
								}
							}
						};
					default:
						throw new AssertionError();
				}
			} else {
				client.onPreShell(channel.getSession().getAttribute(SessionAuthorizedKeysAuthenticator.AUTHORIZED_KEY).getComment());
				// todo: tty not supported
				return new InvertedShellWrapper(new ProcessShell("/system/bin/sh", "-c", command));
			}
		});
		s.setShellFactory((channel) -> {
			client.onPreShell(channel.getSession().getAttribute(SessionAuthorizedKeysAuthenticator.AUTHORIZED_KEY).getComment());
			if (channel.getEnvironment().getEnv().containsKey(Environment.ENV_TERM)) {
				return new InvertedShellWrapper(new ForkptyShell());
			} else {
				return new InvertedShellWrapper(new ProcessShell("/system/bin/sh"));
			}
		});
		return s;
	}

	public static class DirectHost {
		private final SshServer server;

		private DirectHost(Client client) throws IOException {
			final SharedPreferences prefs = HostSettings.getSharedPreferences();
			final String portStr = prefs.getString("direct_port", null);
			if (portStr == null) throw new IllegalArgumentException();
			final int port = Integer.parseInt(portStr);
			server = configureServer(prefs, client);
			server.setSessionFactory(new SessionFactory(server) {
				@Override
				public void sessionCreated(IoSession ioSession) throws Exception {
					super.sessionCreated(ioSession);
					client.onDirectSessionCreated(ioSession.getId(), ioSession.getRemoteAddress());
				}

				@Override
				public void sessionClosed(IoSession ioSession) throws Exception {
					client.onDirectSessionClosed(ioSession.getId());
					super.sessionClosed(ioSession);
				}
			});
			server.setPort(port);
			server.start();
		}

		public void stop() throws IOException {
			server.stop();
		}
	}

	public static DirectHost createDirect(Client client) throws IOException {
		return new DirectHost(client);
	}

	public static class RelayHost {
		private static class ConnectRelayParams {
			public String slot;
			public String host;
		}

		private static final AttributeRepository.AttributeKey<ConnectRelayParams> RELAY_PARAMS = new AttributeRepository.AttributeKey<>();

		public static final Pattern SLOT_PATTERN = Pattern.compile("[0-9A-Fa-f]+");
		public static final Pattern RELAY_HOST_PATTERN = Pattern.compile("[0-9A-Za-z.-]*");

		private final SshServer server;
		private final IoConnector connector;

		private RelayHost(Client client) {
			final SharedPreferences prefs = HostSettings.getSharedPreferences();
			server = configureServer(prefs, client);
			server.setServerProxyAcceptor((session, buffer) -> {
				final String s = new String(buffer.array(), buffer.rpos(), buffer.available(), StandardCharsets.US_ASCII);
				final int headersEnd = s.indexOf("\r\n\r\n");
				if (headersEnd == -1) return false;
				final String headers = s.substring(0, headersEnd + 2);
				if (!HEADER_GOOD.matcher(headers).find()) throw new ProtocolException("Relay responded with " + headers);
				buffer.rpos(buffer.rpos() + headersEnd + 4);
				return true;
			});
			server.setSessionFactory(new SessionFactory(server) {
				@Override
				public void sessionCreated(IoSession ioSession) throws Exception {
					final ConnectRelayParams params = ((AttributeRepository) ioSession.getAttribute(AttributeRepository.class)).getAttribute(RELAY_PARAMS);
					ioSession.writeBuffer(new ByteArrayBuffer((
							"GET /accept?slot=" + params.slot + " HTTP/1.1\r\n" +
									"Host: " + params.host + "\r\n" +
									"User-Agent: te\r\n" +
									"Connection: upgrade\r\n" +
									"Upgrade: websocket\r\n" +
									"\r\n"
					).getBytes()));
					super.sessionCreated(ioSession);
					client.onRelaySessionCreated(ioSession.getId());
				}

				@Override
				public void sessionClosed(IoSession ioSession) throws Exception {
					client.onRelaySessionClosed(ioSession.getId());
					super.sessionClosed(ioSession);
				}
			});
			HelpersAccess.checkConfig(server);
			connector = server.getIoServiceFactory().createConnector(server.getSessionFactory());
		}

		public void acceptSlot(String slot, Client client) {
			if (!SLOT_PATTERN.matcher(slot).matches()) throw new IllegalArgumentException();
			final SharedPreferences prefs = HostSettings.getSharedPreferences();
			final String relayHost = prefs.getString("relay_custom_host", null);
			if (relayHost == null) throw new IllegalArgumentException();
			if (!RELAY_HOST_PATTERN.matcher(relayHost).matches()) throw new IllegalArgumentException();
			final ConnectRelayParams params = new ConnectRelayParams();
			params.slot = slot;
			params.host = relayHost;
			connector.connect(new InetSocketAddress(relayHost, 80), AttributeRepository.ofKeyValuePair(RELAY_PARAMS, params), null).addListener((future) -> {
				final IoSession ioSession = future.getSession();
				if (ioSession == null) {
					Log.v("w", "RelayHost acceptSlot connect", future.getException());
					client.onRelayConnectFail(slot);
				} else {
					client.onRelayConnectSuccess(slot, ioSession.getId());
				}
			});
		}

		public void stop() throws IOException {
			connector.close();
			server.close();
		}
	}

	public static RelayHost createRelay(Client client) {
		return new RelayHost(client);
	}

}

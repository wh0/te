package w.te.rear;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.eclipse.jgit.diff.EditList;
import org.eclipse.jgit.diff.HistogramDiff;
import org.eclipse.jgit.diff.RawText;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.dircache.DirCache;
import org.eclipse.jgit.dircache.DirCacheBuildIterator;
import org.eclipse.jgit.dircache.DirCacheBuilder;
import org.eclipse.jgit.dircache.DirCacheEditor;
import org.eclipse.jgit.dircache.DirCacheEntry;
import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.errors.BinaryBlobException;
import org.eclipse.jgit.errors.DirCacheNameConflictException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.CommitBuilder;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectInserter;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.RefUpdate;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.pack.PackConfig;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.TreeFilter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import w.te.R;
import w.te.low.CharSequenceIO;
import w.te.low.KeepableTempFile;
import w.te.low.ShowUserException;

// somehow the developer was convinced that using the usual working directory and index
// would lead to ruin, so he set out to develop his own system.

public class Working {

	private static boolean initialized;
	public static void init() {
		if (initialized) return;

		try {
			final Path tempDir = Globals.applicationContext.getFilesDir().toPath().resolve(".working");
			Files.createDirectories(tempDir);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		initialized = true;
	}

	private static final int TYPE_CLEAN = 0;
	private static final int TYPE_ABSENT = 1;
	private static final int TYPE_FILE = 2;

	private static CharSequence readBlob(ObjectReader or, ObjectId id) throws IOException {
		final ObjectLoader ol = or.open(id);
		try (InputStream is = ol.openStream()) {
			return CharSequenceIO.readInputStream(is, Math.toIntExact(ol.getSize()));
		}
	}

	private static void writeTempFile(KeepableTempFile k, CharSequence data) throws IOException {
		CharSequenceIO.writeOutputStream(k.getOutputStream(), data);
	}

	private static void writePath(Path path, CharSequence data) throws IOException {
		CharSequenceIO.writeOutputStream(Files.newOutputStream(path), data);
	}

	private static ObjectId idForPathLeaf(ObjectReader or, String path, ObjectId tree) throws IOException {
		// don't pass repo to TreeWalk or it will try to use attributes
		try (TreeWalk tw = TreeWalk.forPath(or, path, tree)) {
			if (tw == null) {
				return null;
			} else {
				if (tw.isSubtree()) {
					return null;
				} else {
					return tw.getObjectId(0);
				}
			}
		}
	}

	public static CharSequence read(String repoName, String path) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				final int type;
				final String src;
				try (Cursor c = db.query("working", new String[]{"type", "src"},
						"repo = ? AND path = ?", new String[]{repoName, path},
						null, null, null)) {
					if (c.moveToFirst()) {
						type = c.getInt(c.getColumnIndexOrThrow("type"));
						src = c.getString(c.getColumnIndexOrThrow("src"));
					} else {
						type = TYPE_CLEAN;
						src = "";
					}
				}
				final CharSequence result;
				if (type == TYPE_CLEAN) {
					try (Repository repo = Repos.getRepo(repoName)) {
						final ObjectId head = repo.exactRef(Constants.HEAD).getObjectId();
						if (head == null) {
							result = "";
						} else {
							try (ObjectReader or = repo.newObjectReader()) {
								final RevTree headTree;
								try (RevWalk rw = new RevWalk(or)) {
									headTree = rw.parseCommit(head).getTree();
								}
								final ObjectId id = idForPathLeaf(or, path, headTree);
								if (id == null) {
									result = "";
								} else {
									result = readBlob(or, id);
								}
							}
						}
					}
				} else if (type == TYPE_ABSENT) {
					result = "";
				} else if (type == TYPE_FILE) {
					final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
					result = CharSequenceIO.readInputStream(Files.newInputStream(temp), Math.toIntExact(Files.size(temp)));
				} else {
					throw new IllegalStateException();
				}
				db.setTransactionSuccessful();
				return result;
			} finally {
				db.endTransaction();
			}
		}
	}

	public static void write(String repoName, String path, CharSequence data) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				final int type;
				final String src;
				try (Cursor c = db.query("working", new String[]{"type", "src"},
						"repo = ? AND path = ?", new String[]{repoName, path},
						null, null, null)) {
					if (c.moveToFirst()) {
						type = c.getInt(c.getColumnIndexOrThrow("type"));
						src = c.getString(c.getColumnIndexOrThrow("src"));
					} else {
						type = TYPE_CLEAN;
						src = "";
					}
				}
				if (type == TYPE_FILE) {
					final String name = src;
					final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(name);
					writePath(temp, data);
				} else {
					// might be nice to check for conflict here, but we might be saving in our last
					// breath and aren't able work this out with the user
					try (KeepableTempFile k = new KeepableTempFile(Globals.applicationContext.getFilesDir().toPath().resolve(".working"), repoName + "-", ".txt")) {
						final String name = k.path.getFileName().toString();
						final ContentValues cv = new ContentValues(4);
						cv.put("repo", repoName);
						cv.put("path", path);
						cv.put("type", TYPE_FILE);
						cv.put("src", name);
						db.replaceOrThrow("working", null, cv);
						writeTempFile(k, data);
						k.keep();
					}
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	public static final int STATUS_CLEAN = 0;
	public static final int STATUS_MODIFIED = 1;
	public static final int STATUS_ADDED = 2;
	public static final int STATUS_DELETED = 3;
	public static final int STATUS_GONE = 4;

	public static final long LAST_COMMIT_UNKNOWN = Long.MIN_VALUE;

	public static class Item {
		public String path;
		public int status;
		public long committedSize;
		public long lastCommit;
		public long modifiedSize;
		public long lastModified;
	}

	public interface Client {
		void onList(List<Item> list);
		void onLastCommit(String path, long lastCommit);
		void onGiveUp();
		void onGone(String path);
		void onDeleted(String path);
		void onReset(String path);
		boolean isCurrent(int cookie);
	}

	public static void delete(String repoName, String path, Client client) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			final ObjectId baseId;
			try {
				try (Repository repo = Repos.getRepo(repoName)) {
					final ObjectId head = repo.exactRef(Constants.HEAD).getObjectId();
					if (head == null) {
						baseId = null;
					} else {
						try (ObjectReader or = repo.newObjectReader()) {
							final ObjectId headTree;
							try (RevWalk rw = new RevWalk(or)) {
								final RevCommit headCommit = rw.parseCommit(head);
								headTree = headCommit.getTree();
							}
							baseId = idForPathLeaf(or, path, headTree);
						}
					}
				}
				final int type;
				final String src;
				try (Cursor c = db.query("working", new String[]{"type", "src"},
						"repo = ? AND path = ?", new String[]{repoName, path},
						null, null, null)) {
					if (c.moveToFirst()) {
						type = c.getInt(c.getColumnIndexOrThrow("type"));
						src = c.getString(c.getColumnIndexOrThrow("src"));
					} else {
						type = TYPE_CLEAN;
						src = "";
					}
				}
				if (baseId == null) {
					db.delete("working", "repo = ? AND path = ?", new String[]{repoName, path});
				} else {
					final ContentValues cv = new ContentValues(4);
					cv.put("repo", repoName);
					cv.put("path", path);
					cv.put("type", TYPE_ABSENT);
					cv.put("src", "");
					db.replaceOrThrow("working", null, cv);
				}
				if (type == TYPE_FILE) {
					final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
					Files.delete(temp);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
			if (baseId == null) {
				client.onGone(path);
			} else {
				client.onDeleted(path);
			}
		}
	}

	public static void reset(String repoName, String path, Client client) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				final int type;
				final String src;
				try (Cursor c = db.query("working", new String[]{"type", "src"},
						"repo = ? AND path = ?", new String[]{repoName, path},
						null, null, null)) {
					if (c.moveToFirst()) {
						type = c.getInt(c.getColumnIndexOrThrow("type"));
						src = c.getString(c.getColumnIndexOrThrow("src"));
					} else {
						type = TYPE_CLEAN;
						src = "";
					}
				}
				db.delete("working", "repo = ? AND path = ?", new String[]{repoName, path});
				if (type == TYPE_FILE) {
					final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
					Files.delete(temp);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
			client.onReset(path);
		}
	}

	private static final int LAST_COMMIT_MAX_DEPTH = 100;

	private static class LastCommitFilter extends TreeFilter {

		public static int TREE_CHANGE = 0;
		public static int TREE_PREV = 1;
		public static int TREE_WANT = 2;

		@Override
		public boolean include(TreeWalk walker) throws MissingObjectException, IncorrectObjectTypeException, IOException {
			throw new UnsupportedOperationException();
		}

		@Override
		public int matchFilter(TreeWalk walker) throws IOException {
			if (FileMode.MISSING.equals(walker.getRawMode(TREE_WANT))) {
				// if we could determine that TREE_WANT is eof, we could throw StopWalkException here
				// don't care
				return 1;
			}
			if (walker.getRawMode(TREE_CHANGE) == walker.getRawMode(TREE_PREV) && walker.idEqual(TREE_CHANGE, TREE_PREV)) {
				// no changes
				return 1;
			}
			if (FileMode.TREE.equals(walker.getRawMode(TREE_CHANGE))) {
				// check for changes within
				return -1;
			}
			// changed
			return 0;
		}

		@Override
		public boolean shouldBeRecursive() {
			return true;
		}

		@Override
		public TreeFilter clone() {
			throw new UnsupportedOperationException();
		}

	}

	public static void list(String repoName, Client client, int cookie) throws IOException {
		try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				try (
						final Repository repo = Repos.getRepo(repoName);
						final ObjectReader or = repo.newObjectReader();
						final RevWalk rw = new RevWalk(or);
				) {
					final Map<String, Item> items = new TreeMap<>();
					final ObjectId head = repo.exactRef(Constants.HEAD).getObjectId();
					final RevCommit current;

					if (head != null) {
						current = rw.parseCommit(head);

						// load the repo's stuff
						final RevTree headTree = current.getTree();
						try (TreeWalk tw = new TreeWalk(or)) {
							tw.setRecursive(true);
							tw.reset(headTree);
							while (tw.next()) {
								final Item item = new Item();
								item.path = tw.getPathString();
								item.status = STATUS_CLEAN;
								items.put(item.path, item);
								final ObjectId id = tw.getObjectId(0);
								item.committedSize = or.getObjectSize(id, ObjectReader.OBJ_ANY);
								item.lastCommit = LAST_COMMIT_UNKNOWN;
							}
						}
					} else {
						current = null;
					}

					// apply changes from working
					try (Cursor c = db.query("working", new String[]{"path", "type", "src"},
							"repo = ?", new String[]{repoName},
							null, null, null)) {
						final int pathIndex = c.getColumnIndexOrThrow("path");
						final int typeIndex = c.getColumnIndexOrThrow("type");
						final int srcIndex = c.getColumnIndexOrThrow("src");
						while (c.moveToNext()) {
							final String path = c.getString(pathIndex);
							final int type = c.getInt(typeIndex);
							final String src = c.getString(srcIndex);
							if (type == TYPE_ABSENT) {
								final Item item = items.get(path);
								item.status = STATUS_DELETED;
							} else if (type == TYPE_FILE) {
								final Item item;
								if (items.containsKey(path)) {
									item = items.get(path);
									item.status = STATUS_MODIFIED;
								} else {
									item = new Item();
									item.path = path;
									item.status = STATUS_ADDED;
									items.put(path, item);
								}
								final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
								item.modifiedSize = Files.size(temp);
								item.lastModified = Files.getLastModifiedTime(temp).toMillis();
							}
						}
					}

					if (head != null) {
						// find the last commits for items from the repo
						final DirCache want = DirCache.read(or, current.getTree());

						client.onList(new ArrayList<>(items.values()));
						// from now on, we don't touch items anymore

						try (TreeWalk tw = new TreeWalk(or)) {
							tw.setRecursive(true);
							tw.setFilter(new LastCommitFilter());
							RevCommit change = current;
							for (int i = 0; i < LAST_COMMIT_MAX_DEPTH; i++) {
								if (!client.isCurrent(cookie)) break;

								final RevCommit prev;
								if (change.getParentCount() == 0) {
									// first commit in this branch. nothing existed before it.
									prev = null;
									tw.reset(change.getTree());
									tw.addTree(new EmptyTreeIterator());
								} else {
									prev = change.getParent(0);
									rw.parseHeaders(prev);
									tw.reset(change.getTree(), prev.getTree());
								}
								tw.addTree(new DirCacheIterator(want));
								final DirCacheBuilder wantBuilder = want.builder();
								tw.addTree(new DirCacheBuildIterator(wantBuilder));
								// 0: change, 1: prev, 2: want, 3: want_builder

								// walk these trees
								while (tw.next()) {
									client.onLastCommit(tw.getPathString(), change.getCommitTime() * 1000L);
								}
								wantBuilder.finish();

								if (want.getEntryCount() == 0) {
									// finished searching
									break;
								}

								if (prev == null) {
									// no more commits
									break;
								}

								change = prev;
							}
						}
						client.onGiveUp();
					} else {
						client.onList(new ArrayList<>(items.values()));
						client.onGiveUp();
					}
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	private static String addEntropy(String template) {
		if (!template.contains("{{entropy}}")) return template;
		final SecureRandom rng = new SecureRandom();
		final String entropy = String.format("%08x%08x%08x%08x%08x",
				rng.nextInt(),
				rng.nextInt(),
				rng.nextInt(),
				rng.nextInt(),
				rng.nextInt());
		return template.replace("{{entropy}}", entropy);
	}

	public static void commit(String repoName) throws IOException, ShowUserException {
		try (SQLiteDatabase db = new Repos.Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				final List<Path> deleteTemp = new ArrayList<>();
				try (
						final Repository repo = Repos.getRepo(repoName);
						final ObjectReader or = repo.newObjectReader();
						final RevWalk rw = new RevWalk(or);
				) {
					final RevCommit parent;
					final DirCache dc;
					final ObjectId head = repo.exactRef(Constants.HEAD).getObjectId();
					if (head != null) {
						parent = rw.parseCommit(head);
						dc = DirCache.read(or, parent.getTree());
					} else {
						parent = null;
						dc = DirCache.newInCore();
					}
					final DirCacheEditor dce = dc.editor();
					try (ObjectInserter oi = repo.newObjectInserter()) {
						try (Cursor c = db.query("working", new String[]{"path", "type", "src"},
								"repo = ?", new String[]{repoName},
								null, null, null)) {
							while (c.moveToNext()) {
								final String path = c.getString(c.getColumnIndexOrThrow("path"));
								final int type = c.getInt(c.getColumnIndexOrThrow("type"));
								final String src = c.getString(c.getColumnIndexOrThrow("src"));
								if (type == TYPE_ABSENT) {
									dce.add(new DirCacheEditor.DeletePath(path));
								} else if (type == TYPE_FILE) {
									final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
									deleteTemp.add(temp);
									final ObjectId blob = oi.insert(Constants.OBJ_BLOB, Files.size(temp), Files.newInputStream(temp));
									dce.add(new DirCacheEditor.PathEdit(path) {
										@Override
										public void apply(DirCacheEntry ent) {
											ent.setObjectId(blob);
											ent.setFileMode(FileMode.REGULAR_FILE);
										}
									}.setReplace(false));
								} else {
									throw new IllegalStateException();
								}
							}
						}

						try {
							dce.finish();
						} catch (DirCacheNameConflictException e) {
							throw new ShowUserException(R.string.error_commit_conflict, e.getPath1(), e.getPath2());
						}

						final ObjectId tree = dc.writeTree(oi);
						if (parent != null && parent.getTree().equals(tree)) {
							Log.v("w", "Working trees are the same, skipping");
						} else {
							final SharedPreferences prefs = CommitSettings.getSharedPreferences();
							final String userName = prefs.getString("user_name", "dummy-user");
							final String userEmail = prefs.getString("user_email", "none");
							final String messageTemplate = prefs.getString("message", "dummy commit\n\nX-Te-Entropy: {{entropy}}\n");
							final String message = addEntropy(messageTemplate);

							final CommitBuilder cb = new CommitBuilder();
							cb.setTreeId(tree);
							if (parent != null) {
								cb.setParentId(parent);
							}
							final PersonIdent pi = new PersonIdent(userName, userEmail);
							cb.setAuthor(pi);
							cb.setCommitter(pi);
							cb.setMessage(message);
							final ObjectId commit = oi.insert(cb);

							oi.flush();

							final RefUpdate ru = repo.updateRef(Constants.HEAD);
							ru.setNewObjectId(commit);
							final RefUpdate.Result r = ru.update(rw);
							switch (r) {
								case NO_CHANGE:
								case NEW:
								case FAST_FORWARD:
								case FORCED:
									break;
								default:
									throw new IOException("RefUpdate.Result " + r);
							}
						}

						db.delete("working", "repo = ?", new String[]{repoName});
					}
				}

				for (final Path temp : deleteTemp) {
					Files.delete(temp);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	public static boolean isClean(SQLiteDatabase db, String repoName) {
		final long numItems = DatabaseUtils.queryNumEntries(db, "working", "repo = ?", new String[]{repoName});
		return numItems == 0;
	}

	public static class DiffResult {
		public RawText committed, working;
		public EditList editList;
	}

	public static DiffResult diff(String repoName, String path) throws IOException, BinaryBlobException {
		try (SQLiteDatabase db = new Repos.Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				final RawText committed, working;
				try (Repository repo = Repos.getRepo(repoName)) {
					final ObjectId head = repo.exactRef(Constants.HEAD).getObjectId();
					if (head == null) {
						committed = RawText.EMPTY_TEXT;
					} else {
						try (ObjectReader or = repo.newObjectReader()) {
							final RevTree headTree;
							try (RevWalk rw = new RevWalk(or)) {
								headTree = rw.parseCommit(head).getTree();
							}
							final ObjectId id = idForPathLeaf(or, path, headTree);
							if (id == null) {
								committed = RawText.EMPTY_TEXT;
							} else {
								committed = RawText.load(or.open(id), PackConfig.DEFAULT_BIG_FILE_THRESHOLD);
							}
						}
					}
				}
				final int type;
				final String src;
				try (Cursor c = db.query("working", new String[]{"type", "src"},
						"repo = ? AND path = ?", new String[]{repoName, path},
						null, null, null)) {
					if (c.moveToFirst()) {
						type = c.getInt(c.getColumnIndexOrThrow("type"));
						src = c.getString(c.getColumnIndexOrThrow("src"));
					} else {
						type = TYPE_CLEAN;
						src = "";
					}
				}
				if (type == TYPE_CLEAN) {
					working = committed;
				} else if (type == TYPE_ABSENT) {
					working = RawText.EMPTY_TEXT;
				} else if (type == TYPE_FILE) {
					final Path temp = Globals.applicationContext.getFilesDir().toPath().resolve(".working").resolve(src);
					working = new RawText(temp.toFile());
				} else {
					throw new IllegalStateException();
				}
				db.setTransactionSuccessful();
				final EditList editList = new HistogramDiff().diff(RawTextComparator.DEFAULT, committed, working);
				final DiffResult result = new DiffResult();
				result.working = working;
				result.committed = committed;
				result.editList = editList;
				return result;
			} finally {
				db.endTransaction();
			}
		}
	}

}

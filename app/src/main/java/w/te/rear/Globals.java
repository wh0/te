package w.te.rear;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

public class Globals {

	public static Context applicationContext;
	public static final Handler mainHandler = new Handler(Looper.getMainLooper());

}

package w.te.rear;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.FileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import w.te.R;
import w.te.low.ShowUserException;

public class Repos {

	// todo: allow more names?
	private static final Pattern NAME_PATTERN = Pattern.compile("[0-9A-Za-z][0-9A-Za-z-]*");

	private static boolean initialized;
	public static void init() {
		if (initialized) return;

		try {
			final Path systemDir = Globals.applicationContext.getCacheDir().toPath().resolve("system");
			Files.createDirectories(systemDir);
			final Path systemConfigPath = systemDir.resolve("gitconfig");
			Files.copy(Globals.applicationContext.getResources().openRawResource(R.raw.gitconfig), systemConfigPath, StandardCopyOption.REPLACE_EXISTING);
			FS.DETECTED.setGitSystemConfig(systemConfigPath.toFile());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		initialized = true;
	}

	public static class Helper extends SQLiteOpenHelper {

		public Helper() {
			super(Globals.applicationContext, "repos", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE repos (name TEXT PRIMARY KEY)");
			db.execSQL("CREATE TABLE working (repo TEXT, path TEXT, type INTEGER, src TEXT, PRIMARY KEY (repo, path))");
			db.execSQL("CREATE TABLE backups (uri TEXT PRIMARY KEY, name TEXT, from_id TEXT, to_id TEXT)");
			db.execSQL("CREATE TABLE last_backup (repo TEXT PRIMARY KEY, last_id TEXT)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			throw new UnsupportedOperationException();
		}

	}

	public static List<String> list() {
		final List<String> result = new ArrayList<>();
		try (SQLiteDatabase db = new Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				try (Cursor c = db.query("repos", new String[]{"name"},
						null, null,
						null, null, null)) {
					final int nameIndex = c.getColumnIndexOrThrow("name");
					while (c.moveToNext()) {
						result.add(c.getString(nameIndex));
					}
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
		return result;
	}

	public static void create(String repoName) throws IOException, ShowUserException {
		final Matcher matcher = NAME_PATTERN.matcher(repoName);
		if (!matcher.matches()) throw new IllegalArgumentException();
		try (SQLiteDatabase db = new Helper().getReadableDatabase()) {
			db.beginTransaction();
			try {
				if (DatabaseUtils.queryNumEntries(db, "repos", "name = ?", new String[]{repoName}) != 0) throw new ShowUserException(R.string.error_create_repo_exist);
				final ContentValues cv = new ContentValues(1);
				cv.put("name", repoName);
				db.insertOrThrow("repos", null, cv);
				try (Repository repo = getRepo(repoName)) {
					repo.create(true);
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	public static void delete(String repoName) throws IOException, ShowUserException {
		try (SQLiteDatabase db = new Helper().getWritableDatabase()) {
			db.beginTransaction();
			try {
				if (DatabaseUtils.queryNumEntries(db, "working", "repo = ?", new String[]{repoName}) != 0) throw new ShowUserException(R.string.error_delete_repo_working_exist);
				if (DatabaseUtils.queryNumEntries(db, "backups", "uri = ?", new String[]{Backups.getBackupUri(repoName)}) != 0) throw new ShowUserException(R.string.error_delete_repo_backup_exist);
				if (db.delete("repos", "name = ?", new String[]{repoName}) != 1) throw new IllegalStateException();
				db.delete("last_backup", "repo = ?", new String[]{repoName});
				FileUtils.delete(Globals.applicationContext.getFilesDir().toPath().resolve(repoName).toFile(), FileUtils.RECURSIVE);
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
	}

	public static Repository getRepo(String repoName) throws IOException {
		final Path dir = Globals.applicationContext.getFilesDir().toPath().resolve(repoName);
		return new FileRepository(dir.toFile());
	}

}

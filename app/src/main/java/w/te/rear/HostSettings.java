package w.te.rear;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.ExecutionException;

import w.te.R;
import w.te.low.ExternalObservable;
import w.te.low.ShowUserException;

public class HostSettings {

	public static final ExternalObservable authorizedKeysObservable = new ExternalObservable();

	public static void configurePreferenceManager(PreferenceManager pm) {
		pm.setSharedPreferencesName("host");
	}

	public static SharedPreferences getSharedPreferences() {
		return Globals.applicationContext.getSharedPreferences("host", Context.MODE_PRIVATE);
	}

	public static void registerToken(SharedPreferences prefs) throws IOException, ShowUserException, ExecutionException, InterruptedException {
		final String senderId = prefs.getString("relay_custom_sender_id", null);
		final String host = prefs.getString("relay_custom_host", null);
		final String nickname = prefs.getString("relay_nickname", null);
		final String knock = prefs.getString("relay_knock", null);
		// bail if we haven't set up our relay yet
		if (senderId == null || host == null || nickname == null || knock == null) throw new ShowUserException(R.string.error_relay_config);
		// reinitialize app if sender id changed
		FirebaseApp fa = FirebaseApp.getInstance();
		FirebaseOptions fo = fa.getOptions();
		if (!senderId.equals(fo.getGcmSenderId())) {
			fo = new FirebaseOptions.Builder(fo).setGcmSenderId(senderId).build();
			Tasks.await(FirebaseMessaging.getInstance().deleteToken());
			fa.delete();
			FirebaseApp.initializeApp(Globals.applicationContext, fo);
		}
		final String token = Tasks.await(FirebaseMessaging.getInstance().getToken());
		final URL u = new URL("https://" + host + "/register-token" +
				"?nickname=" + URLEncoder.encode(nickname, "UTF-8") +
				"&knock=" + URLEncoder.encode(knock, "UTF-8") +
				"&token=" + URLEncoder.encode(token, "UTF-8"));
		final HttpURLConnection c = (HttpURLConnection) u.openConnection();
		c.setDoInput(false);
		c.setRequestMethod("POST");
		final int status = c.getResponseCode();
		if (status != 200) throw new ProtocolException();
		prefs.edit().putLong("relay_last_registered", System.currentTimeMillis()).apply();
	}

	public static void unregister(SharedPreferences prefs) throws IOException, ExecutionException, InterruptedException {
		Tasks.await(FirebaseMessaging.getInstance().deleteToken());
		prefs.edit().remove("relay_last_registered").apply();
	}

	public static void resetTokenRegistration(SharedPreferences prefs) {
		prefs.edit().remove("relay_last_registered").apply();
	}

}

package w.te.rear;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class CommitSettings {

	public static void configurePreferenceManager(PreferenceManager pm) {
		pm.setSharedPreferencesName("commit");
	}

	public static SharedPreferences getSharedPreferences() {
		return Globals.applicationContext.getSharedPreferences("commit", Context.MODE_PRIVATE);
	}

}

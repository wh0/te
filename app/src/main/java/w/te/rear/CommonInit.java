package w.te.rear;

import android.content.Context;

public class CommonInit {

	public static void initFront(Context applicationContext) {
		if (Globals.applicationContext != null) return;

		Globals.applicationContext = applicationContext;

		Formality.init();
	}

	private static boolean initialized;

	public static void init() {
		if (initialized) return;

		System.setProperty("user.home", Globals.applicationContext.getCacheDir().toPath().resolve("home").toString());

		initialized = true;
	}

}

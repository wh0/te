package w.te.rear;

import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.StreamingAead;

import org.eclipse.jgit.internal.storage.dfs.DfsObjDatabase;
import org.eclipse.jgit.internal.storage.dfs.DfsOutputStream;
import org.eclipse.jgit.internal.storage.dfs.DfsPackDescription;
import org.eclipse.jgit.internal.storage.dfs.DfsReaderOptions;
import org.eclipse.jgit.internal.storage.dfs.DfsReftableDatabase;
import org.eclipse.jgit.internal.storage.dfs.DfsRepository;
import org.eclipse.jgit.internal.storage.dfs.DfsRepositoryBuilder;
import org.eclipse.jgit.internal.storage.dfs.DfsRepositoryDescription;
import org.eclipse.jgit.internal.storage.dfs.ReadableChannel;
import org.eclipse.jgit.internal.storage.pack.PackExt;
import org.eclipse.jgit.lib.RefDatabase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CoasterRepository extends DfsRepository {

	private static byte[] getAad(PackExt ext) {
		return ("v6-" + ext.toString()).getBytes();
	}

	private static class CoasterReadableChannel implements ReadableChannel {
		private final SeekableByteChannel plaintextChannel;

		public CoasterReadableChannel(Path file, StreamingAead primitive, byte[] aad) throws IOException {
			final SeekableByteChannel ciphertextChannel = Files.newByteChannel(file);
			try {
				plaintextChannel = primitive.newSeekableDecryptingChannel(ciphertextChannel, aad);
			} catch (GeneralSecurityException e) {
				ciphertextChannel.close();
				throw new IOException(e);
			}

			// Force the decrypter to parse the header so we can compute size information.
			try {
				plaintextChannel.read(ByteBuffer.allocate(1));
				plaintextChannel.position(0);
			} catch (IOException e) {
				plaintextChannel.close();
				throw e;
			}
		}

		@Override
		public long position() throws IOException {
			return plaintextChannel.position();
		}

		@Override
		public void position(long newPosition) throws IOException {
			System.err.println("%%% CoasterReadableChannel position " + newPosition);
			plaintextChannel.position(newPosition);
		}

		@Override
		public long size() throws IOException {
			final long size = plaintextChannel.size();
			System.err.println("%%% CoasterReadableChannel size " + size);
			return size;
		}

		@Override
		public int blockSize() {
			return -1;
		}

		@Override
		public void setReadAheadBytes(int bufferSize) throws IOException {
			System.err.println("%%% CoasterReadableChannel setReadAheadBytes " + bufferSize);
		}

		@Override
		public int read(ByteBuffer dst) throws IOException {
			System.err.println("%%% CoasterReadableChannel read " + dst.remaining());
			return plaintextChannel.read(dst);
		}

		@Override
		public boolean isOpen() {
			return plaintextChannel.isOpen();
		}

		@Override
		public void close() throws IOException {
			System.err.println("%%% CoasterReadableChannel close " + this);
			plaintextChannel.close();
		}
	}

	private static class CoasterOutputStream extends DfsOutputStream {
		private final Path file;
		private final StreamingAead primitive;
		private final byte[] aad;
		private final OutputStream plaintextStream;
		private SeekableByteChannel readChannel;
		private final byte[] shadowBuffer;
		private long shadowEnd;

		public CoasterOutputStream(Path file, StreamingAead primitive, byte[] aad, int shadowCap) throws IOException {
			this.file = file;
			this.primitive = primitive;
			this.aad = aad;
			final OutputStream ciphertextStream = Files.newOutputStream(file);
			try {
				plaintextStream = primitive.newEncryptingStream(ciphertextStream, aad);
			} catch (GeneralSecurityException e) {
				ciphertextStream.close();
				throw new IOException(e);
			}
			shadowBuffer = new byte[shadowCap];
			shadowEnd = 0;
		}

		private void startRead() throws IOException {
			if (readChannel != null) return;
			System.err.println("%%% CoasterOutputStream startRead starting");
			plaintextStream.flush();
			final SeekableByteChannel ciphertextChannel = Files.newByteChannel(file);
			try {
				readChannel = primitive.newSeekableDecryptingChannel(ciphertextChannel, aad);
			} catch (GeneralSecurityException e) {
				ciphertextChannel.close();
				throw new IOException(e);
			}
		}

		private void stopRead() throws IOException {
			if (readChannel == null) return;
			System.err.println("%%% CoasterOutputStream stopRead stopping");
			readChannel.close();
			readChannel = null;
		}

		@Override
		public int blockSize() {
			return -1;
		}

		@Override
		public void write(byte[] buf, int off, int len) throws IOException {
			stopRead();
			System.err.println("%%% CoasterOutputStream write " + len);
			plaintextStream.write(buf, off, len);

			final int shadowCap = shadowBuffer.length;
			if (len > shadowCap) {
				// For big writes, skip all but the last lap.
				final int skip = len - shadowCap;
				off += skip;
				len -= skip;
				shadowEnd += skip;
			}
			final int start = (int) (shadowEnd % shadowCap);
			shadowEnd += len;
			if (start + len > shadowCap) {
				final int beforeWrap = shadowCap - start;
				System.arraycopy(buf, off, shadowBuffer, start, beforeWrap);
				off += beforeWrap;
				len -= beforeWrap;
				System.arraycopy(buf, off, shadowBuffer, 0, len);
			} else {
				System.arraycopy(buf, off, shadowBuffer, start, len);
			}
		}

		@Override
		public void flush() throws IOException {
			plaintextStream.flush();
		}

		@Override
		public void close() throws IOException {
			stopRead();
			Arrays.fill(shadowBuffer, (byte) 99);
			plaintextStream.close();
		}

		@Override
		public int read(long position, ByteBuffer buf) throws IOException {
			System.err.println("%%% CoasterOutputStream read " + buf.remaining() + " at " + position);
			final int shadowCap = shadowBuffer.length;
			final long shadowLength = Math.min(shadowCap, shadowEnd);
			final long shadowStart = shadowEnd - shadowLength;
			final long requestEnd = position + buf.remaining();

			if (requestEnd <= shadowStart) {
				System.err.println("%%% CoasterOutputStream read forwarding");
				startRead();
				readChannel.position(position);
				return readChannel.read(buf);
			}

			int total = 0;
			if (position < shadowStart) {
				final int take = (int) (shadowStart - position);
				System.err.println("%%% CoasterOutputStream read taking " + take + " from readChannel");
				startRead();
				readChannel.position(position);
				final int origLimit = buf.limit();
				buf.limit(buf.position() + take);
				final int amount;
				try {
					amount = readChannel.read(buf);
				} finally {
					buf.limit(origLimit);
				}
				total += amount;
				if (amount < take) return total;
				position += amount;
			}

			final int start = (int) (position % shadowCap);
			final long takeEnd = Math.min(requestEnd, shadowEnd);
			int take = (int) (takeEnd - position);
			System.err.println("%%% CoasterOutputStream read taking " + take + " from shadowBuffer");
			total += take;
			if (start + take > shadowCap) {
				final int beforeWrap = shadowCap - start;
				buf.put(shadowBuffer, start, beforeWrap);
				take -= beforeWrap;
				buf.put(shadowBuffer, 0, take);
			} else {
				buf.put(shadowBuffer, start, take);
			}

			return total;
		}
	}

	private static class PackAccumulator {
		public DfsPackDescription pack;
		public boolean uncommitted;
	}

	private static class CoasterObjectDatabase extends DfsObjDatabase {
		private static final Pattern PACKFILE_PATTERN = Pattern.compile("(coaster-([^-]+)-[^.]+)\\.(.+)");
		private static final Map<String, PackExt> PACK_EXTS;

		static {
			final PackExt[] exts = PackExt.values();
			PACK_EXTS = new HashMap<>(exts.length);
			for (PackExt ext : exts) {
				PACK_EXTS.put(ext.getExtension(), ext);
			}
		}

		private class SizeCheck extends DfsPackDescription {
			public SizeCheck(DfsRepositoryDescription repoDesc, String name, DfsObjDatabase.PackSource packSource) {
				super(repoDesc, name, packSource);
			}

			@Override
			public long getFileSize(PackExt ext) {
				final long size = super.getFileSize(ext);
				if (size > 0) return size;
				try (ReadableChannel rc = openFile(this, ext)) {
					final long checkedSize = rc.size();
					System.err.println("%%% SizeCheck getFileSize " + this + " ext " + ext + " " + checkedSize);
					setFileSize(ext, checkedSize);
					return checkedSize;
				} catch (IOException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}

		private final Path packDir;
		private final StreamingAead primitive;
		private final int shadowCap;

		public CoasterObjectDatabase(DfsRepository repository, Path packDir, KeysetHandle keys, int shadowCap) throws GeneralSecurityException {
			super(repository, new DfsReaderOptions());
			this.packDir = packDir;
			this.primitive = keys.getPrimitive(StreamingAead.class);
			this.shadowCap = shadowCap;
		}

		@Override
		protected DfsPackDescription newPack(PackSource source) throws IOException {
			final String name = "coaster-" + source + '-' + UUID.randomUUID().toString();
			final DfsPackDescription packDesc = new SizeCheck(getRepository().getDescription(), name, source);
			final Path uncommittedFile = packDir.resolve(packDesc.getFileName(PackExt.INDEX) + ".uncommitted");
			System.err.println("%%% CoasterObjectDatabase newPack " + uncommittedFile);
			Files.createFile(uncommittedFile);
			return packDesc;
		}

		@Override
		protected void commitPackImpl(Collection<DfsPackDescription> desc, Collection<DfsPackDescription> replaces) throws IOException {
			for (DfsPackDescription pack : desc) {
				System.err.println("%%% CoasterObjectDatabase commitPackImpl desc " + pack);
				Files.delete(packDir.resolve(pack.getFileName(PackExt.INDEX) + ".uncommitted"));
			}
			if (replaces != null) {
				for (DfsPackDescription pack : replaces) {
					System.err.println("%%% CoasterObjectDatabase commitPackImpl replaces " + pack);
					final Path uncommittedFile = packDir.resolve(pack.getFileName(PackExt.INDEX) + ".uncommitted");
					Files.createFile(uncommittedFile);
					for (PackExt ext : PackExt.values()) {
						if (pack.hasFileExt(ext)) {
							Files.delete(packDir.resolve(pack.getFileName(ext)));
						}
					}
					Files.delete(uncommittedFile);
				}
			}
		}

		@Override
		protected void rollbackPack(Collection<DfsPackDescription> desc) {
			for (DfsPackDescription pack : desc) {
				System.err.println("%%% CoasterObjectDatabase rollbackPack " + pack);
				boolean success = true;
				for (PackExt ext : PackExt.values()) {
					try {
						Files.deleteIfExists(packDir.resolve(pack.getFileName(ext)));
					} catch (IOException e) {
						success = false;
						e.printStackTrace();
					}
				}
				if (success) {
					try {
						Files.deleteIfExists(packDir.resolve(pack.getFileName(PackExt.INDEX) + ".uncommitted"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		@Override
		protected List<DfsPackDescription> listPacks() throws IOException {
			System.err.println("%%% CoasterObjectDatabase listPacks " + packDir);
			final Map<String, PackAccumulator> packs = new HashMap<>();
			int numCommitted = 0;
			try (DirectoryStream<Path> dir = Files.newDirectoryStream(packDir)) {
				for (Path path : dir) {
					final Matcher m = PACKFILE_PATTERN.matcher(path.getFileName().toString());
					if (!m.matches()) continue;
					final String name = m.group(1);
					final PackAccumulator pa;
					if (!packs.containsKey(name)) {
						pa = new PackAccumulator();
						final PackSource source = PackSource.valueOf(m.group(2));
						pa.pack = new SizeCheck(getRepository().getDescription(), name, source);
						packs.put(name, pa);
						numCommitted++;
					} else {
						pa = packs.get(name);
					}
					final String extension = m.group(3);
					if ("idx.uncommitted".equals(extension)) {
						pa.uncommitted = true;
						numCommitted--;
					} else {
						final PackExt ext = PACK_EXTS.get(extension);
						if (ext == null) throw new IllegalArgumentException();
						pa.pack.addFileExt(ext);
					}
				}
			}
			final ArrayList<DfsPackDescription> result = new ArrayList<>(numCommitted);
			for (Map.Entry<String, PackAccumulator> entry : packs.entrySet()) {
				if (entry.getValue().uncommitted) continue;
				result.add(entry.getValue().pack);
			}
			return result;
		}

		@Override
		protected ReadableChannel openFile(DfsPackDescription desc, PackExt ext) throws FileNotFoundException, IOException {
			final String fileName = desc.getFileName(ext);
			final Path path = packDir.resolve(fileName);
			System.err.println("%%% CoasterObjectDatabase openFile " + path); // %%%
			return new CoasterReadableChannel(path, primitive, getAad(ext));
		}

		@Override
		protected DfsOutputStream writeFile(DfsPackDescription desc, PackExt ext) throws IOException {
			final String fileName = desc.getFileName(ext);
			final Path path = packDir.resolve(fileName);
			System.err.println("%%% CoasterObjectDatabase writeFile " + path); // %%%
			return new CoasterOutputStream(path, primitive, getAad(ext), shadowCap);
		}
	}

	private static class CoasterRefDatabase extends DfsReftableDatabase {
		public CoasterRefDatabase(DfsRepository repo) {
			super(repo);
		}
	}

	public static class Builder extends DfsRepositoryBuilder<Builder, CoasterRepository> {
		private Path packDir;
		private KeysetHandle keys;
		private int shadowCap;

		@Override
		public CoasterRepository build() throws IOException {
			Objects.requireNonNull(getRepositoryDescription());
			Objects.requireNonNull(getPackDir());
			Objects.requireNonNull(getKeys());
			final CoasterRepository repo;
			try {
				repo = new CoasterRepository(this);
			} catch (GeneralSecurityException e) {
				throw new IOException(e);
			}
			return repo;
		}

		public Path getPackDir() {
			return packDir;
		}

		public KeysetHandle getKeys() {
			return keys;
		}

		public int getShadowCap() {
			return shadowCap;
		}

		public Builder setPackDir(Path packDir) {
			this.packDir = packDir;
			return this;
		}

		public Builder setKeys(KeysetHandle keys) {
			this.keys = keys;
			return this;
		}

		public Builder setShadowCap(int shadowCap) {
			this.shadowCap = shadowCap;
			return this;
		}
	}

	private final CoasterObjectDatabase objdb;
	private final CoasterRefDatabase refdb;

	protected CoasterRepository(Builder builder) throws GeneralSecurityException {
		super(builder);
		objdb = new CoasterObjectDatabase(this, builder.getPackDir(), builder.getKeys(), builder.getShadowCap());
		refdb = new CoasterRefDatabase(this);
	}

	@Override
	public DfsObjDatabase getObjectDatabase() {
		return objdb;
	}

	@Override
	public void create(boolean bare) throws IOException {
		Files.createDirectories(objdb.packDir);
		super.create(bare);
	}

	@Override
	public RefDatabase getRefDatabase() {
		return refdb;
	}

}

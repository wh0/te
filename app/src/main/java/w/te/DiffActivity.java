package w.te;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawText;
import org.eclipse.jgit.errors.BinaryBlobException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import w.te.databinding.ActivityDiffBinding;
import w.te.rear.CommonInit;
import w.te.rear.Repos;
import w.te.rear.Working;

public class DiffActivity extends Activity {

	private ActivityDiffBinding binding;
	private String repoName;
	private String path;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		CommonInit.initFront(getApplicationContext());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			CommonInit.init();
			Repos.init();
			Working.init();
		});
		super.onCreate(savedInstanceState);
		repoName = getIntent().getStringExtra("repo");
		path = getIntent().getStringExtra("path");
		setTitle(repoName + "/" + path);
		setTaskDescription(new ActivityManager.TaskDescription(repoName + "/" + path));
		binding = ActivityDiffBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		AsyncTask.SERIAL_EXECUTOR.execute(() -> {
			final Working.DiffResult result;
			try {
				result = Working.diff(repoName, path);
			} catch (IOException | BinaryBlobException e) {
				throw new RuntimeException(e);
			}
			final SpannableStringBuilder pretty = new SpannableStringBuilder();
			final SpanColorFormatter formatter = SpanColorFormatter.into(pretty);
			try {
				formatter.format(result.editList, result.committed, result.working);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			formatter.flip();
			runOnUiThread(() -> {
				binding.diff.setText(pretty);
			});
		});
		// todo: might need a refresh button
	}

	private static class SpanColorFormatter extends DiffFormatter {

		private final SpannableStringBuilder dst;
		private final ByteArrayOutputStream buf;

		private SpanColorFormatter(SpannableStringBuilder dst, ByteArrayOutputStream buf) {
			super(buf);
			this.dst = dst;
			this.buf = buf;
		}

		public static SpanColorFormatter into(SpannableStringBuilder dst) {
			return new SpanColorFormatter(dst, new ByteArrayOutputStream());
		}

		public void flip() {
			final String s;
			try {
				s = buf.toString("UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
			dst.append(s);
			buf.reset();
		}

		@Override
		protected void writeHunkHeader(int aStartLine, int aEndLine, int bStartLine, int bEndLine) throws IOException {
			flip();
			final int start = dst.length();
			super.writeHunkHeader(aStartLine, aEndLine, bStartLine, bEndLine);
			flip();
			final int end = dst.length();
			dst.setSpan(new ForegroundColorSpan(0xff009688), start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE); // teal 500
		}

		@Override
		protected void writeRemovedLine(RawText text, int line) throws IOException {
			flip();
			final int start = dst.length();
			super.writeRemovedLine(text, line);
			flip();
			final int end = dst.length();
			dst.setSpan(new ForegroundColorSpan(0xfff44336), start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE); // red 500
		}

		@Override
		protected void writeAddedLine(RawText text, int line) throws IOException {
			flip();
			final int start = dst.length();
			super.writeAddedLine(text, line);
			flip();
			final int end = dst.length();
			dst.setSpan(new ForegroundColorSpan(0xff4caf50), start, end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE); // green 500
		}
	}

}

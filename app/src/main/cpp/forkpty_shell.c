#include <android/log.h>
#include <errno.h>
#include <fcntl.h>
#include <jni.h>
#include <pty.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

static jfieldID f_masterFd;
static jfieldID f_procPid;
static jfieldID f_exitStatus;

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
	__android_log_print(ANDROID_LOG_VERBOSE, "w", "loading forkpty_shell"); // %%%
	JNIEnv *env;
	jint rv = (*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6);
	if (rv != JNI_OK) {
		__android_log_print(ANDROID_LOG_ERROR, "w", "GetEnv returned 0x%x", rv);
		return JNI_ERR;
	}
	jclass c_ForkptyShell = (*env)->FindClass(env, "w/te/low/ForkptyShell");
	f_masterFd = (*env)->GetFieldID(env, c_ForkptyShell, "masterFd", "I");
	f_procPid = (*env)->GetFieldID(env, c_ForkptyShell, "procPid", "I");
	f_exitStatus = (*env)->GetFieldID(env, c_ForkptyShell, "exitStatus", "I");
	__android_log_print(ANDROID_LOG_VERBOSE, "w", "forkpty_shell loaded ok"); // %%%
	return JNI_VERSION_1_6;
}

static void throw_io_errno(JNIEnv *env, const char *message) {
	char buf[1024];
	snprintf(buf, sizeof(buf), "%s: %s", message, strerror(errno));
	jclass c_IOException = (*env)->FindClass(env, "java/io/IOException");
	(*env)->ThrowNew(env, c_IOException, buf);
}

JNIEXPORT void JNICALL
Java_w_te_low_ForkptyShell_startNative(JNIEnv *env, jobject instance) {
	int amaster;
	pid_t pid = forkpty(&amaster, NULL, NULL, NULL);
	if (pid == -1) {
		throw_io_errno(env, "forkpty");
		return;
	} else if (pid == 0) {
		// child
		if (execl("/system/bin/sh", "-sh", NULL) == -1) {
			perror("execl");
			_exit(1);
		}
	} else {
		// parent
		(*env)->SetIntField(env, instance, f_masterFd, amaster);
		(*env)->SetIntField(env, instance, f_procPid, pid);
		__android_log_print(ANDROID_LOG_VERBOSE, "w", "startNative ok fd %d pid %d", amaster, pid); // %%%
	}
}

static int convert_status(int wstatus) {
	// this is what ojluni does
	if (WIFEXITED(wstatus)) {
		return WEXITSTATUS(wstatus);
	} else if (WIFSIGNALED(wstatus)) {
		return WSTOPSIG(wstatus) + 0x80;
	} else {
		return wstatus;
	}
}

JNIEXPORT jboolean JNICALL
Java_w_te_low_ForkptyShell_checkStatusNative(JNIEnv *env, jobject instance) {
	pid_t pid = (*env)->GetIntField(env, instance, f_procPid);
	int wstatus;
	int rv = waitpid(pid, &wstatus, WNOHANG);
	if (rv == -1) {
		throw_io_errno(env, "waitpid");
		return JNI_FALSE;
	} else if (rv == 0) {
		return JNI_TRUE;
	}
	int exit_status = convert_status(wstatus);
	(*env)->SetIntField(env, instance, f_exitStatus, exit_status);
	__android_log_print(ANDROID_LOG_VERBOSE, "w", "checkStatusNative exited %d", exit_status); // %%%
	return JNI_FALSE;
}

JNIEXPORT void JNICALL
Java_w_te_low_ForkptyShell_destroyNative(JNIEnv *env, jobject instance) {
	pid_t pid = (*env)->GetIntField(env, instance, f_procPid);
	int rv = kill(pid, SIGKILL);
	if (rv == -1) {
		throw_io_errno(env, "kill");
		return;
	}
	int wstatus;
	rv = TEMP_FAILURE_RETRY(waitpid(pid, &wstatus, 0));
	if (rv == -1) {
		throw_io_errno(env, "waitpid");
		return;
	}
	int exit_status = convert_status(wstatus);
	(*env)->SetIntField(env, instance, f_exitStatus, exit_status);
}

JNIEXPORT void JNICALL
Java_w_te_low_ForkptyShell_signalNative(JNIEnv *env, jobject instance, jint sig) {
	pid_t pid = (*env)->GetIntField(env, instance, f_procPid);
	int rv = killpg(pid, sig);
	if (rv == -1) {
		throw_io_errno(env, "killpg");
		return;
	}
}

JNIEXPORT void JNICALL
Java_w_te_low_ForkptyShell_setWindowSizeNative(JNIEnv *env, jobject instance, jint row, jint col, jint xpixel, jint ypixel) {
	int master = (*env)->GetIntField(env, instance, f_masterFd);
	struct winsize ws = {
		.ws_row = (unsigned short) row,
		.ws_col = (unsigned short) col,
		.ws_xpixel = (unsigned short) xpixel,
		.ws_ypixel = (unsigned short) ypixel,
	};
	int rv = ioctl(master, TIOCSWINSZ, &ws);
	if (rv == -1) {
		throw_io_errno(env, "ioctl");
		return;
	}
}

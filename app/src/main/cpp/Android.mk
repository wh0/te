LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := forkpty_shell
LOCAL_SRC_FILES := forkpty_shell.c
LOCAL_LDLIBS := -llog
include $(BUILD_SHARED_LIBRARY)

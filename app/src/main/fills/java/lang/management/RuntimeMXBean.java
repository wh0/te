package java.lang.management;

public interface RuntimeMXBean {

	String getName();

}

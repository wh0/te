# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# sshd
-keep class org.apache.sshd.common.io.nio2.Nio2ServiceFactoryFactory { <init>(); }
-keep class org.apache.sshd.common.util.security.bouncycastle.BouncyCastleSecurityProviderRegistrar { <init>(); }
-dontwarn net.i2p.crypto.eddsa.**
-dontwarn org.ietf.jgss.**

# jgit
-keep class org.eclipse.jgit.internal.JGitText { <init>(); }

# Only want the shrinking
-dontobfuscate

#-printconfiguration

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# instrumentation tests
-keep public class kotlin.**
-keep public class androidx.lifecycle.**
-keep public class com.google.common.util.concurrent.** { public *; }
-keep class w.te.R$* { public static <fields>; }

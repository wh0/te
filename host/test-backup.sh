#!/bin/sh -eux
tmp=/tmp/te-test-backup
rm -rf "$tmp"
mkdir "$tmp"
adb shell run-as w.te.dev base64 //data/data/w.te.dev/cache/tour.bundle.gpg | base64 -di >"$tmp/tour.bundle.gpg"
(
export GNUPGHOME="$tmp/gnupg"
cd "$tmp"
mkdir gnupg
chmod 700 gnupg
gpg --decrypt --batch --passphrase hunter2 --output tour.bundle tour.bundle.gpg
git clone tour.bundle
cat tour/sample.txt
)
rm -rf "$tmp"

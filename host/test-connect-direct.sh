#!/bin/sh -eux
adb forward tcp:20522 tcp:20522
tmp=/tmp/te-test-connect
rm -rf "$tmp"
mkdir "$tmp"
cp app/src/testConfig/id_rsa app/src/testConfig/res/raw/ssh_host_rsa_key "$tmp"
chmod 600 "$tmp/id_rsa" "$tmp/ssh_host_rsa_key"
cat >"$tmp/config" <<EOF
Host target
HostName 127.0.0.1
Port 20522
User testing
IdentitiesOnly yes
IdentityFile $tmp/id_rsa
UserKnownHostsFile $tmp/known_hosts
EOF
cat >"$tmp/known_hosts" <<EOF
[127.0.0.1]:20522 $(ssh-keygen -yf "$tmp/ssh_host_rsa_key")
EOF
(
export GIT_SSH_COMMAND="ssh -vvv -F $tmp/config"
cd "$tmp"
git clone target:tour
cd tour
echo "updated." >> sample.txt
git config user.name testing
git config user.email none
git commit -a -m "update sample"
git push origin master
)
ssh -F "$tmp/config" target <<EOF
rmdir /data/data/w.te.dev/cache/test-connect-running
exit
EOF
adb forward --remove tcp:20522
rm -rf "$tmp"

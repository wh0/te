This is Te, which is an Android app.
It stores Git repositories on your phone.

Te didn't have the best upbringing, but it always tries its hardest.

---

Additional permission under GNU GPL version 3 section 7

If you modify this program, or any covered work, by linking or
combining it with Firebase Cloud Messaging, the copyright holders
grant you additional permission to convey the resulting work.
